
import {
  StackNavigator,
} from 'react-navigation';

import Notifiche from './src/Screen/Notifiche';
import RecipeDetail from './src/Screen/RecipeDetail';
import EventDetail from './src/Screen/EventDetail';
import Home2 from './src/Screen/Home2';
import Ricerca from './src/Screen/Ricerca';
import Registration from './src/Screen/Registration';
import Login from './src/Screen/Login';
import DefaultPreference from 'react-native-default-preference';
import Conferma from './src/Screen/Conferma';
import FirstView from './src/Screen/FirstView';
import FilePage from './src/Screen/FilePage';
import PackageInsert from './src/Screen/PackageInsert';
import Profile from './src/Screen/Profile';
import InfoUser from './src/Screen/InfoUser';
import Intro from './src/Screen/Intro';



GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;




const Dotto = StackNavigator({
  FirstView: { screen: FirstView },
  Registration: { screen: Registration },
  Login: { screen: Login },
  Conferma: { screen: Conferma },
  Home2: { screen: Home2 },
  Ricerca: { screen: Ricerca },
  RecipeDetail: { screen: RecipeDetail },
  EventDetail: { screen: EventDetail },
  FilePage: { screen: FilePage },
  PackageInsert:{ screen: PackageInsert},
  Profile: {screen: Profile},
  InfoUser: {screen: InfoUser},
  Intro: {screen: Intro}
 


},
  {
    headerMode: 'none'
  }
);

export default Dotto


