import React from 'react';
import { View, TextInput, Text, TouchableHighlight, Image, KeyboardAvoidingView, Picker, ScrollView, Dimensions, Platform } from 'react-native';
import DefaultPreference from 'react-native-default-preference';
import { PermissionsAndroid } from 'react-native';

const win = Dimensions.get('window');



export default class RegistrationForm extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            nome: '', cognome: '', email: '', albo: '', confermaMail: '', password: '', confermaPassword: '', message: '',
            data: {}, categories: [], filtro: [], latitude: null, longitude: null, error: null,
        }
        this.requestAccessLocationPermission = this.requestAccessLocationPermission.bind(this)
    }


    async requestAccessLocationPermission() {

        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,

                {
                    'title': 'Dotto',
                    'message': 'Dotto needs access to your Gps location'
                    // 'so you can take awesome pictures.'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use GPS")
            } else {
                console.log("GPS permission denied")
            }
        } catch (err) {
            console.warn(err)
        }
    }

    componentWillMount(props) {

        if (Platform.OS === 'android') {
            this.requestAccessLocationPermission()
        }

        DefaultPreference.get('access_token').then((pippo) => {
            if (pippo !== null && pippo != undefined) {
                this.props.navigation.navigate('Home2')
            }
        })

        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => {
                this.setState({ error: error.message })
                alert(error.message)
            });
        fetch('http://dotto.bincode.it/api/categories', {
            method: 'GET',
        }).then((response) => response.json())
            .then((responseJson) => {
               
                this.setState(() => {
                    
                    Object.keys(responseJson).forEach((i) => {
                        this.state.categories.push(responseJson[i])
                    })
                });
                this.setArray()
            }).catch((error) => {
               
            })
    }


    setArray = () => {
        let categories = this.state.categories;
        categories.map((s, i) => {

            return <Picker.Item key={i} label={s} />

        });
    }


    registration() {
        let email = this.state.email
        let groups = 390
        let password = this.state.password
        let nome = this.state.nome
        let albo = this.state.albo
        let confermaPassword = this.state.confermaPassword
        let data = this.state.data
        fetch('http://dotto.bincode.it/api/users', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                data: {
                    email: email,
                    password: password,
                    name: nome,
                    numero: albo,
                    conferma: confermaPassword
                }
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
              
                this.props.navigation.navigate('Conferma')
            }).catch((error) => {
                Alert.alert(
                    'Errore!!'
                )
            })
  

    }




    render() {
        
        return (
            <ScrollView >
               
                <View style={styles.pageStyle}>
                    <View style={styles.containerImage}>
                        <Image style={styles.bannerStyle} source={require('../../assets/images/logo_app.png')} />
                    </View>
                    <Text style={styles.labelStyle}>Nome</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            underlineColorAndroid={'#25a588'}
                            style={styles.textInputStyle}
                            placeholder={'Nome'}
                            placeholderTextColor={'#dbf0eb'}
                            value={this.state.nome}
                            onChangeText={nome => this.setState({ nome })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Indirizzo email</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'mail'}
                            value={this.state.email}
                            onChangeText={email => this.setState({ email })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Conferma indirizzo email</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'mail'}
                            value={this.state.confermaMail}
                            onChangeText={confermaMail => this.setState({ confermaMail })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>N° Iscrizione albo</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'Iscrizione albo'}
                            value={this.state.albo}
                            onChangeText={albo => this.setState({ albo })}
                        />
                    </View>
                    <View style={styles.viewPickerStyle}>
                        <Text style={styles.labelStyleSpecializzazione}>Specializzazione principale</Text>
                        <Picker
                            //style={styles.viewPickerStyle}
                            itemStyle={{ color: 'white', fontFamily: 'Montserrat-Regular', fontSize: 15 }}
                            selectedValue={this.state.selectedCategories}
                            onValueChange={(itemValue) => this.setState({ selectedCategories: itemValue })}>
                            {this.setArray()}

                        </Picker>
                    </View>
                    <Text style={styles.labelStyle}>Password</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'Password'}
                            value={this.state.password}
                            onChangeText={password => this.setState({ password })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Conferma Password</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'Password'}
                            value={this.state.confermaPassword}
                            onChangeText={confermaPassword => this.setState({ confermaPassword })}
                        />
                    </View>
                    <TouchableHighlight style={styles.button} onPress={() => {
                        this.registration()
                    }}>
                        <Text style={{ color: '#ffffff', fontFamily: 'Montserrat-Bold' }}>Registrami</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.buttonTwo}>
                        <Text style={{ color: '#b8e1d8', fontFamily: 'Montserrat-Bold', fontSize: 9 }}>Accedi ad una versione limitata valida 30 giorni</Text>
                    </TouchableHighlight>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: '2%', marginBottom: '4%' }}>
                        <Text style={{ color: '#b8e1d8', fontFamily: 'Montserrat-Bold', fontSize: 9 }}>Hai già un account?</Text>
                        <TouchableHighlight underlayColor='#25a588' onPress={() => {
                            this.props.navigation.navigate('Login')
                        }
                        }
                        ><Text style={{ height: 50, color: '#e94e1b', fontFamily: 'Montserrat-Bold', fontSize: 9 }}>Accedi</Text></TouchableHighlight>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = {

    pageStyle: {
        backgroundColor: '#25a588',
        //height: win.height*1,
        alignItems: 'center',
    },

    button: {
        width: '80%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e94e1b',
        padding: 10,
        marginTop: '3%',
        borderRadius: 25
    },

    buttonTwo: {
        width: '80%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#ffffff',
        borderWidth: 1,
        padding: 10,
        marginTop: '1%',
        borderRadius: 25
    },

    containerImage: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '3%',
        marginBottom: '5%',
        height: 70,
        width: '50%'
    },

    bannerStyle: {
        resizeMode: 'contain',
        height: '70%',

    },

    labelStyle: {
        color: '#b8e1d8',
        marginTop: '3%'
    },

    labelStyleSpecializzazione: {
        color: '#b8e1d8',
        marginTop: '5%'
    },

    viewTextInputStyle: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#b8e1d8',
        borderBottomWidth: 1,
    },

    textInputStyle: {
        //marginTop: -10,
        marginBottom: '1%',
        width: '100%',
        fontFamily: 'Montserrat-Bold',
        color: '#ffffff',
        textAlign: 'center',

    },

    viewPickerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#b8e1d8',
        borderBottomWidth: 1,
        width: '100%',
        height: 80,
        backgroundColor: '#1f8b72',
        //flexWrap: "wrap"
    },
   
    
}
