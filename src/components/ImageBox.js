import React from 'react';
import { View, Image,Text, TouchableOpacity,ScrollView } from 'react-native';

export default class ImageBox extends React.Component  {
   
    constructor(props){
        super(props)
    }

  render(){
    let image=this.props.params.image
    let accetto=this.props.params.actions[0].label
    let declino=this.props.params.actions[1].label
    let nameEvent=this.props.params.title
    let url=this.props.params.url
    return (
        <ScrollView>
           
            <Image style={styles.imageStyle} source={{uri: image}} />
            <View style={styles.viewS}>
                <Image style={styles.imgS} source={require('../../assets/images/calendar.png')} />
                <Text style={styles.textS}>{nameEvent}</Text>
                <Text style={styles.testoStyle}>Venerdi 18 Marzo 2018</Text>
            </View>
            <View style={styles.view}>
            <View style={styles.buttonStyle}><Text style={styles.testo}>{declino}</Text></View>
            <TouchableOpacity style={styles.buttonStyle1} onPress={()=> this.props.navigation.navigate('Quattro')}><Text style={styles.testo}>{accetto}</Text></TouchableOpacity>
        </View>
        </ScrollView>
    );
}
};
const styles = {
    viewStyle: {
        height:'40%',
    },
    imageStyle: {
        resizeMode: 'contain',
        marginLeft:'3%',
        marginRight:'3%',
        width: '94%',
        aspectRatio:1,
    },
    viewS:{
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginLeft:'3%',
        marginRight:'3%', 
        width:'94%',
        aspectRatio: 6,
        borderLeftColor: '#D3D2D2',
        borderLeftWidth:1,
        borderRightColor: '#D3D2D2',
        borderRightWidth:1,
        backgroundColor:'#ffffff'
    },
    imgS: {
        resizeMode: 'contain',
        marginLeft: '4%',
        height: 30,
        width: 35,        
    },

    textS: {
        alignItems: 'center',
        marginLeft: '18%',
        marginTop: -30,
        fontSize: 10,
        fontFamily: 'Montserrat-Regular'
    },

    testoStyle: {
        marginLeft: '18%',
        fontFamily: 'Montserrat-Bold'
    },
    view: {
        height: '40%',
        justifyContent: 'center',
        flexDirection: 'row',
    },

    buttonStyle : {
        marginBottom: 0,
        height: 40,
        width: '47%',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#D3D2D2',
        marginLeft: '3%',
        backgroundColor:'#ffffff'
    },

    buttonStyle1: {
        height: 40,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        width: '47%',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#D3D2D2',
        marginRight: '3%',
        borderBottomWidth: 4,
        borderBottomColor: '#64dd17',
        backgroundColor:'#ffffff'
    },

    testo:{
        fontSize: 15,
        fontFamily: 'Montserrat-Regular',
        color:'#2a2a2a'
    },
};