import React from 'react';
import { View, Text, TouchableOpacity,ScrollView } from 'react-native';
import Image from 'react-native-remote-svg';
import { Dimensions } from 'react-native';

const Box = () => (
            <View style={styles.containerStyle}>
                <TouchableOpacity style={styles.touchableContainerStyle} >
                    <Image style={[styles.imageStyle, {aspectRatio : 0.92}]} source={require('../../assets/images/prontuario.png')} />
                    <View style={styles.textViewStyle}>
                        <Text style={styles.titleTextBoldTouchableStyle}>
                            prontuario 
                        </Text>
                        <Text style={styles.baseTextTouchableStyle}>
                            evoluto
                        </Text>
                    </View>
                    <Text style={styles.dateTextTouchableStyle}>aggiornato 12/12/2017</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.touchableContainerSecondStyle}>
                    <Image style={[styles.imageStyle, {aspectRatio : 0.85}]} source={require('../../assets/images/calcolatorimedici.png')} />
                    <View style={styles.textViewStyle}>
                        <Text style={styles.titleTextBoldTouchableStyle}>
                            calcolatori 
                        </Text>
                        <Text style={styles.baseTextTouchableStyle}>
                            medici
                        </Text>
                    </View>
                    <Text style={styles.dateTextTouchableStyle}>aggiornato 12/12/2017</Text>
                </TouchableOpacity>
            </View>
)


const window= Dimensions.get('window');
const styles = {
    containerStyle: {
        flex: 1,
        maxHeight:'40%',
        width:'100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#e3e4e8',
        borderBottomWidth: 3,
        borderRadius: 8,
        backgroundColor: '#F8F8F8'
    },
    touchableContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '40%',
        marginLeft:'2%'
    },
    touchableContainerSecondStyle:{
        justifyContent: 'center',
        alignItems: 'center',
        width: '40%',
        marginLeft:'12%'

    },

    titleTextBoldTouchableStyle: {
        color: 'black',
        marginTop: '6%',
        fontFamily: 'Montserrat-Bold',
        fontSize: 15,
    },
    baseTextTouchableStyle: {
        marginTop: '6%',
        fontSize: 15,
        marginLeft:'2%',
        fontFamily: 'Montserrat-Regular'
    },

    dateTextTouchableStyle: {
        fontSize: 9,
        fontFamily: 'Montserrat-Regular',
        color: '#ACACAC',
    },

    imageStyle: {
        height: '45%',
        marginTop: '3%',
    },

    textViewStyle: {
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center',
        marginLeft: '8%',
        marginRight: '3%'
    }

};

export default Box;