import React from 'react';
import { Component, StyleSheet, Text, View, Image, TouchableHighlight, Animated } from 'react-native'; //Step 1

class Panel extends React.Component {
    constructor(props) {
        super(props);

        this.icons = {     
            'up': require('../../assets/images/freccia-sotto.png'),
            'down': require('../../assets/images/freccia-sotto.png')
        };

        this.state = {      
            expanded: true,
            animation: new Animated.Value(120),
            minHeight: 120,
            isPressed: false
        };
    }



    toggle() {

        let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;


            this.setState({
                isPressed: false
            })

        this.state.animation.setValue(initialValue);  
        Animated.timing(     
            this.state.animation,
            {
                toValue: finalValue,
                duration: 300
            }
        ).start();  

      
    }

    _setMaxHeight(event) {
        this.setState({
            maxHeight: event.nativeEvent.layout.height - 100
        }, () => {if (this.state.isPressed) this.toggle()});
    }

    _setMinHeight(event) {
        this.setState({
            minHeight: event.nativeEvent.layout.height + 20
        });
    }


    render() {
        let icon = this.icons['down'];

        if (this.state.expanded) {
            icon = this.icons['up'];  
        }

       
        return (
            <Animated.View
                style={[styles.container, { height: this.state.animation }]}>
                <TouchableHighlight
                    style={styles.button}
                    onPress={() => this.setState({expanded: !this.state.expanded, isPressed: true})}
                    underlayColor="#f1f1f1">
                    <View style = {styles.container}>
                        <View style={styles.titleContainer}>

                            <Text style={styles.title} >{this.props.title}</Text>

                        </View>
                        {(!this.state.expanded) ?
                            <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                                {this.props.children}
                            </View>
                            :
                            <View style={styles.body}  onLayout={this._setMaxHeight.bind(this)}>

                                <View numberOfLines={6} style = {styles.bodyText}>{this.props.children}</View>
                            </View>
                        }

                    </View>
                </TouchableHighlight>

            </Animated.View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        overflow: 'hidden',
        minHeight: 90,
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: '4%',
    },
    title: {
        color: '#bdbdbd',
        fontFamily: 'Montserrat-Regular',
        marginBottom: 4,
        fontSize: 12,
        marginLeft:-5
    },
    buttonImage: {
        resizeMode: 'contain',
        marginTop: '1%',
        width: '10%',
        height: 20,
        marginRight: '5%',
    },
    body: {
        padding: 10,
        paddingTop: 0,
    },
    bodyText: {
        height: '100%',
        width: '100%',
    }
});
export default Panel;

