import React from 'react';

class EventRegister {

    static shared_instance = undefined;

    static getInstance() {
       
        if (this.shared_instance == undefined) {
            this.shared_instance = new EventRegister();
            this
                .shared_instance
                .initialize();
        }

        return this.shared_instance;
    }

    initialize() {
        
        this.pendingEvents = []
    }

    trackEvent(objectType, action, objectID,latitude,longitude) {
      
        var newEvent = [];
        newEvent["object"] = objectType;
        newEvent["action"] = action;
        newEvent["object_id"] = objectID;
        newEvent["lat"] = latitude;
        newEvent["lng"] = longitude;
        var ts = new Date().getUTCMilliseconds();
        newEvent["deviceid"] = String(ts);

        this
            .pendingEvents
            .push(newEvent);

        this.sendRequest();
    }

    sendRequest() {
        if (this.pendingEvents.length > 0) {
            fetch('http://dotto.bincode.it/api/users-actions-register', {
                method: 'POST',
                headers: new Headers({
                    "Authorization": `Bearer ${global.userToken}`
                })
            }).then((response) => response.json()).then((responseJson) => {

                this.pendingEvents = [];
            }).catch((error) => {
                console.log(error)
                
            })
        }
    }

}

export default EventRegister;