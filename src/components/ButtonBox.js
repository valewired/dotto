import React from 'react';
import {View, TouchableOpacity, Text } from 'react-native';
import Image from 'react-native-remote-svg';

export default class ButtonBox extends React.Component {
    render(){

    return (
        <View style={styles.view}>
            <View style={styles.buttonStyle}><Text style={styles.testo}>DECLINO</Text></View>
            <TouchableOpacity style={styles.buttonStyle1} onPress={()=> this.props.navigation.navigate('Quattro')}><Text style={styles.testo}>ACCETTO</Text></TouchableOpacity>
        </View>
    );
}
};

const styles={
    
view: {
        height: 50,
        justifyContent: 'center',
        flexDirection: 'row',
    },

    buttonStyle : {
        marginBottom: 0,
        height: 40,
        width: '47%',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#D3D2D2',
        marginLeft: '3%',
        backgroundColor:'#ffffff'
    },

    buttonStyle1: {
        height: 40,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        width: '47%',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#D3D2D2',
        marginRight: '3%',
        borderBottomWidth: 4,
        borderBottomColor: '#64dd17',
        backgroundColor:'#ffffff'
    },

    testo:{
        fontSize: 15,
        fontFamily: 'Montserrat-Regular',
        color:'#2a2a2a'
    },
};