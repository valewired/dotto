import React from 'react';
import { Modal, View, Image, Text, TouchableOpacity, TextInput, TouchableHighlight, KeyboardAvoidingView, SectionList, Platform} from 'react-native';
import Search from '../components/Search';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { NavigationActions } from 'react-navigation';


export default class HeaderSearch extends React.Component {

    constructor(props) {
        super(props);
        this.state = { text: '', message: '', data: '',  searchDelegate: props.searchCallback, closeDelegate: props.closeCallback, profileDelegate: props.profileCallback, closeProfileDelegate: props.closeProfileCallback};
    
        this.backToHome = this.backToHome.bind(this);
        this.navigationSystem = this.props.navigation;
    }

    backToHome() {
        let resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'Home2'})
            ]
        })
        this.navigationSystem.dispatch(resetAction)
    }

    render() {
        data = this.state.data
        return (
            <View>
                <View style={[styles.containerStyle, (Platform.OS == 'ios') ? styles.containerStyleiOS : styles.containerStyleAnd]}>
                    <TouchableHighlight style={styles.iconMenuContainerStyle} underlayColor={'#25A588'} onPress={() => { this.props.navigation.navigate('Profile')}}>
                        <Image style={styles.iconStyle} source={require('../../assets/images/menu-left.png')} />
                    </TouchableHighlight>
                        <TouchableHighlight underlayColor={'#25A588'} style={styles.logoContainerStyle} onPress={() =>{ this.backToHome() }}>
                        <Image style={styles.logoStyle} source={require('../../assets/images/dotto_banner.png')} />
                        </TouchableHighlight>
                   
                    <TouchableHighlight style={styles.iconSearchContainerStyle} underlayColor={'#25A588'} onPress={() => {
                        this.state.searchDelegate();
                    }}>
                        <Image style={styles.iconStyle} source={require('../../assets/images/search-icon.png')} />
                    </TouchableHighlight>
                </View>
               
            </View>
        );
    }
};


const styles = {
    containerStyle: {
        backgroundColor: '#25A588',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 0
    },

    containerStyleiOS: {
        ...ifIphoneX({
            paddingTop: 45,
            height: 95,
        }, {
            paddingTop: 20,
            height: 70,
        })
    },

    containerStyleAnd: {
        height: 50,
    },

    logoContainerStyle: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center'

    },

    logoStyle: {
        resizeMode: 'contain',
        width: '60%',
        height: 25
    },

    iconMenuContainerStyle: {
        width: '25%',
        height: 45,
        justifyContent: 'center',
        alignItems: 'flex-start',

    },

    iconSearchContainerStyle: {
        width: '25%',
        justifyContent: 'center',
        alignItems: 'flex-end',

    },

    iconStyle: {
        resizeMode: 'contain',
        width: '40%',
        height: 20

    },
    iconCancelContainerStyle: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',

    },

    mainContainer: {
        backgroundColor: '#25A588',
        height: 30
    },
    container: {
        backgroundColor: '#25A588',
        //overflow: 'hidden'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: '5%',
    },
    title: {
       
        color: '#ffffff',
        fontFamily: 'Montserrat-Regular'
    },
    menuToolbar: {
        position: 'relative',
        bottom: 0,
        width: '100%',
        flexDirection: 'row', 
        justifyContent: 'space-around', 
        alignItems: 'center', 
        backgroundColor: '#ffffff', 
        elevation: 10, 
        height: 45
    },
    button: {
      
    },
    buttonImage: {
        resizeMode: 'contain',
        backgroundColor:'#25A588',
        marginTop: '1%',
        width: 20,
        //height: 40,
        marginLeft:'70%'
        //marginRight: '10%',
    },
    body: {
        padding: 10,
        paddingTop: 0,
        width:'100%'
       
    }

};
