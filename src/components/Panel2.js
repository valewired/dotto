import React from 'react';
import { Component, StyleSheet, Text, View, Image, TouchableHighlight, Animated } from 'react-native'; //Step 1

class Panel extends React.Component {
    constructor(props) {
        super(props);

        this.icons = {   
            'up': require('../../assets/images/expand_more.png'),
            'down': require('../../assets/images/expand_more.png')
        };

        this.state = {       
            expanded: false,
            animation: new Animated.Value(40),
            minHeight: 40,
            openWebView:  this.props.openPanel
        };

    }

    toggle() {

        let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue = this.state.expanded ? this.state.minHeight : this.props.items.length * 41 + this.state.minHeight;

        this.setState({
            expanded: !this.state.expanded
        });

        this.state.animation.setValue(initialValue);
        Animated.timing(
            this.state.animation,
            {
                toValue: finalValue,
                duration: 300
            }
        ).start();
        
        if (this.props.id !== undefined){
        this.state.openWebView(this.props.items,this.props.id)
        }
    }

    _setMaxHeight(event) {
        this.setState({
            maxHeight: event.nativeEvent.layout.height
        });
        console.log("maxHeight :" + event.nativeEvent.layout.height)
    }

    _setMinHeight(event) {
        this.setState({
            minHeight: event.nativeEvent.layout.height
        });
        console.log("minHeight:" + event.nativeEvent.layout.height)
    }


    render() {
        let icon = this.icons['down'];

        if (this.state.expanded) {
            icon = this.icons['up'];
        }

        //Step 5
        return (
            <Animated.View
                style={[styles.container, { height: this.state.animation }]}>
                <TouchableHighlight
                    style={styles.button}
                    onPress={this.toggle.bind(this)}
                    underlayColor="#f1f1f1">
                    <View style={styles.container} >
                        <View style={styles.titleContainer}>
                            <View style={styles.subTitleContainer}>
                                <Text style={styles.title} >{this.props.title}</Text>
                                <Text style={styles.title2} >{this.props.title2}</Text>
                            </View>
                            <Image
                                style={styles.buttonImage}
                                source={icon}
                                resizeMode={'contain'}
                            ></Image>
                        </View>

                        <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                            {this.props.children}
                        </View>

                    </View>
                </TouchableHighlight>

            </Animated.View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        overflow: 'hidden'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 50,
        backgroundColor: '#e5e5e5',

    },
    subTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    title: {
        marginLeft: '5%',
        color: '#585858',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12
    },
    title2: {
        color: '#585858',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12
    },
    button: {
    },
    buttonImage: {
        resizeMode: 'contain',
        marginTop: '1%',
        width: 10,
        height: 10,
        marginRight: '5%',
    },
    body: {
        paddingTop: 0
    }
});
export default Panel;