import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Image from 'react-native-remote-svg';

export default class Buttons extends React.Component {
    render() {
        return (
            <View style={styles.containerStyle}>
                <View style={styles.touchableFirstStyle}>
                    <Text style={styles.textTouchableButton}>STRUMENTI</Text>
                </View>
                <View style={styles.touchableStyle} >
                    <Text style={styles.textTouchableButton}>NOTIFICHE</Text>
                    <Image style={styles.iconButtonStyle} source={require('../../assets/images/icon-359.png')} />
                </View>

            </View>

        );
    }
};

const styles = {
    containerStyle: {
        backgroundColor: '#ffffff',
        width: '100%',
        height:'100%',
        flexDirection: 'row',
        borderBottomColor: '#e3e4e8',
        borderBottomWidth: 1,
        overflow:'hidden',
    },

    touchableFirstStyle: {
        width: '50%',
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 3,
        borderBottomColor: '#25A588',
    },

    touchableStyle: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },

    textTouchableButton: {
        fontSize: 12,
        color: '#3a3a39',
        fontFamily: 'Montserrat-Regular'
    },

    iconButtonStyle: {
        //resizeMode: 'contain',
        marginLeft: '1%',
        height: 20,
        width: 40,
    },
};
