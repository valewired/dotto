import React from 'react';
import { View, Text, ScrollView, ImageBackground } from 'react-native';
import Image from 'react-native-remote-svg';
import ViewMoreText from 'react-native-view-more-text';
import Panel from '../components/Panel';

export default class CardBox extends React.Component {
    constructor(props) {
        super(props)
    }

    renderViewMore(onPress) {
        return (
            <Text onPress={onPress}></Text>
        )
    }

    renderViewLess(onPress) {
        return (
            <Text onPress={onPress}></Text>
        )
    }

    render() {
        return (
            <View elevation={7} style={styles.cardStyle}>
                <View style={styles.viewTop}>
                    <Text style={styles.textTop}>{this.props.data.name}</Text>
                </View>
                <View style={styles.secondView}>
                    <View style={styles.textView}>
                        <Text style={styles.secondText}>
                            RISORSE  {'\n'}
                            <Text>
                                DISPONIBILI
                    </Text>
                        </Text>
                    </View>
                    <View style={styles.viewImage}>
                        <View style={styles.viewAcronymStyle}>
                            <ImageBackground style={styles.imageTop} source={require('../../assets/images/icon-internet.png')}>
                                <Text style={{ marginLeft: '80%', marginBottom: '60%', fontSize: 10, backgroundColor: 'red', borderRadius: 2, color: '#ffffff' }}>{this.props.data.websites.items.length}</Text>
                            </ImageBackground>
                            <Text style={styles.acronymStyle}>WWW</Text>
                        </View>
                        <View style={styles.viewAcronymStyle}>
                            <ImageBackground style={styles.imageTop} source={require('../../assets/images/icon-documenti.png')}>
                                <Text style={{ marginLeft: '80%', marginBottom: '60%', fontSize: 10 }}>{this.props.data.documents.items.length}</Text>
                            </ImageBackground>
                            <Text style={styles.acronymStyle}>DOC</Text>
                        </View>
                        <View style={styles.viewAcronymStyle}>
                            <ImageBackground style={styles.imageTop} source={require('../../assets/images/icon-calcolatore.png')}>
                                <Text style={{ marginLeft: '80%', marginBottom: '60%', fontSize: 10 }}>{this.props.data.calculators.length}</Text>
                            </ImageBackground>
                            <Text style={styles.acronymStyle}>CALC</Text>

                        </View>
                        <View style={styles.viewAcronymStyle}>
                            <ImageBackground style={styles.imageTop} source={require('../../assets/images/icon-rcp.png')}>
                                <Text style={{ marginLeft: '80%', marginBottom: '60%', fontSize: 10 }}>{this.props.data.calculators.length}</Text>
                            </ImageBackground>
                            <Text style={styles.acronymStyle}>RCP</Text>

                        </View>
                    </View>
                </View>
                <View style={styles.thirdView}>
               
                    
                <Panel title={this.props.data.active_principle.partner_name}>
                <Text style={styles.centerText}>{this.props.data.active_principle.name}  </Text>
                <Text style={styles.centerBoldText}>HyQvia</Text>
                        <Text style={styles.bottomText}>{this.props.data.data.dosage}</Text>
                    </Panel>
                </View>
                <View style={styles.bottomView}>
                    <View style={styles.touchableOpacityLeftBox}>
                        <Image style={styles.imageBottom} source={require('../../assets/images/calendar.png')} />
                        <Text style={styles.textBottom}>POSOLOGIA</Text>
                    </View>
                    <View style={styles.touchableOpacityBox}>
                        <Image style={styles.imageBottom} source={require('../../assets/images/calendar.png')} />
                        <Text style={styles.textBottom}>POSOLOGIA</Text>
                    </View>
                    <View style={styles.touchableOpacityBox}>
                        <Image style={styles.imageBottom} source={require('../../assets/images/calendar.png')} />
                        <Text style={styles.textBottom}>POSOLOGIA</Text>
                    </View>
                </View>
            </View>
        );
    }
};


const styles = {
    cardStyle: {
        marginTop: '3%',
        marginBottom: '3%',
        marginLeft: '4%',
        marginRight: '4%',
        height:320,
        backgroundColor: '#fafafa'
    },

    cardShadowStyle: {
        shadowColor: "#000000",
        shadowOpacity: 3,
        shadowRadius: 6,
        shadowOffset: {
            height: -2,
            width: -4
        }
    },

    viewTop: {
        backgroundColor: '#25A588',
        width: '100%',
        height: '8%',
        justifyContent: 'center',
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3

    },

    secondView: {
        flexDirection: 'row',
        width: '100%',
        height: '20%',
        alignItems: 'center',
        backgroundColor: '#ffffff'
    },

    textTop: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 10,
        marginLeft: '7%',
        color: 'white'
    },

    secondText: {
        fontFamily: 'Montserrat-Regular',
        marginLeft: '4%',
        color: '#25A588',
        marginRight: '4%'
    },

    imageTop: {
        //marginTop:'5%',
        //resizeMode: 'contain',

        width: 27,
        height: 27,
        marginLeft: '5.4%'
    },

    viewImage: {
        flexDirection: 'row',
        width: '55%',
        justifyContent: 'space-between',
        marginLeft: '5%',
    },

    viewAcronymStyle: {
        alignItems: 'center',
        marginTop: '7%'
    },

    acronymStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 8,
        marginTop: '3%',
        marginLeft: '3%'
    },

    thirdView: {
        marginTop: '2%',
       
    },

    centerText: {
        fontFamily: 'Montserrat-Regular',
        marginLeft: '4%',
        fontSize: 12,
        color: '#bdbdbd'
    },

    centerBoldText: {
        fontFamily: 'Montserrat-Bold',
        marginLeft: '4%',
        fontSize: 15
    },

    bottomText: {
        fontFamily: 'Montserrat-Regular',
        marginLeft: '4%',
        marginTop: '7%',
        marginRight: '2%',
        fontSize: 13,
        color: '#959595'
    },

    bottomView: {
        marginTop: '7%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: '30%',
        borderTopWidth: 1,
        borderTopColor: '#d4cece'
    },

    imageBottom: {
        height: 50,
        width: 35,
        //marginLeft:'3%'
    },

    textBottom: {
        fontSize: 10,
        fontFamily: 'Montserrat-Bold',
        color: '#25A588'

    },

    touchableOpacityBox: {
        marginLeft: '17%',
        alignItems: 'center',
    },

    touchableOpacityBox: {
        marginLeft: '17%',
        alignItems: 'center'
    },

    touchableOpacityLeftBox: {
        alignItems: 'center',
    },

    textView: {
        width: '40%'
    },
    continerCircle: {
        height: 70,
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0)',
        overflow: 'hidden',
    },
    circle: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        backgroundColor: '#70d2d1',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        top: 8
    },
}

