import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Image from 'react-native-remote-svg';
import { Dimensions } from 'react-native';

const Box = (props) => (
            <View style={styles.containerStyle}>
                <TouchableOpacity style={styles.touchableContainerStyle} onPress={() => this.props.navigation.navigate('Notifiche')}>
                
                    <Image style={styles.imageStyle} source={require('../../assets/images/pronturario.svg')} />
             
                    <View style={styles.textViewStyle}>
                        <Text style={styles.titleTextBoldTouchableStyle}>
                            prontuario {'\t'}
                        </Text>
                        <Text style={styles.baseTextTouchableStyle}>
                            evoluto
                </Text>
                    </View>
                    <Text style={styles.dateTextTouchableStyle}>aggiornato 12/12/2017</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.touchableContainerStyle}>
                    <Image style={styles.imageStyle} source={require('../../assets/images/calc.svg')} />
                    <View style={styles.textViewStyle}>
                        <Text style={styles.titleTextBoldTouchableStyle}>
                            calcolatori{'\t'}
                        </Text>
                        <Text style={styles.baseTextTouchableStyle}>
                            medici
                </Text>
                    </View>
                    <Text style={styles.dateTextTouchableStyle}>aggiornato 12/12/2017</Text>
                </TouchableOpacity>
            </View>
)


const window= Dimensions.get('window');
const styles = {
    containerStyle: {
        width: '100%',
        height:'35%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#e3e4e8',
        borderBottomWidth: 5,
        borderRadius: 8,
        backgroundColor: '#F8F8F8'
       
    },

    touchableContainerStyle: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center'
    },

    titleTextBoldTouchableStyle: {
        marginTop: '6%',
        fontFamily: 'Montserrat-Bold',
        fontSize: 17,
    },

    baseTextTouchableStyle: {
        marginTop: '6%',
        fontSize: 17,
        fontFamily: 'Montserrat-Regular'
    },

    dateTextTouchableStyle: {
        fontSize: 13,
        fontFamily: 'Montserrat-Regular'
    },

    imageStyle: {
        width: 125,
        height: 125,
        marginTop: '4%',
        backgroundColor: 'yellow'
    },

    textViewStyle: {
        flexDirection: 'row',
        backgroundColor: 'green'
    }

};

export default Box;