import React from 'react';
import { View, Text, SectionList, TouchableOpacity, Button, Platform, Animated, Dimensions } from 'react-native';
import Image from 'react-native-remote-svg';

export default class CardList extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            data: [],
            _rowX : {}
        }
    }

    componentWillMount() {
        this.retrieveRecipies()
    }


    retrieveRecipies() {
        var response = fetch('http://dotto.bincode.it/api/notifications', {
            method: 'GET',
            headers: new Headers({
                "Authorization": `Bearer ${global.userToken}`
            })
        }).then(response => response.json())
            .then(responseJson => {
                if (responseJson.items.length > 0) {
                    var array = []
                    responseJson.items.map(d => array.push({
                        type: d.type,
                        icon: d.icon,
                        id: d.id,
                        title: d.title,
                        short_description: d.short_description,
                        image_src: d.image_src,
                        target: d.target,
                        status: d.status,
                        diff_for_humans: d.diff_for_humans,
                        created_at: d.created_at,
                        productName: d.product.name,
                        companyName: d.company.name
                    }))
                    this.setState({ data: array })
                }
                if (responseJson.error_code != undefined) {
                    console.log("Recipies retrieved!");
                }

            }).catch((error) => {
                console.log("Fallimento chiamata");
               

            });

    }

    deleteItem(item) {
        Animated.timing(this.state._rowX[item.id], {
            toValue  : Dimensions.get('window').width,
            duration : 500
        }).start(() => {
            var array = this.state.data // make a separate copy of the array
            var index = array.indexOf(item)
            array.splice(index, 1);
            this.setState({data: array});
        });
    }

    renderItem = ({ item }) => {
        this.state._rowX[item.id] = new Animated.Value(0)
        return (
            <Animated.View key = {item.id} style = {[{height: 122, overflow: 'hidden', right: this.state._rowX[item.id]}]}>
            <TouchableOpacity style={[(item.type == "Document") ? styles.TouchableOpacityStyle : (item.type == "Website") ? styles.TouchableOpacityYellowStyle : styles.TouchableOpacityRedStyle, styles.globalTouvhableStyle]}>
                <View style={styles.containerTextStyle}>
                    <Text style={styles.firstTextStyle}>
                        {item.title} </Text>
                    <Text style={styles.firstTextStyleSecondPart}>
                        {
                            (item.type == "Document") ?
                                "vuole condividere con te:" :
                                "vuole invitarti a visitare:"
                        }
                    </Text>
                </View>
                <View style={styles.viewContainerFile}>
                    <Image style={styles.fileImageStyle} source={{ uri: item.icon }} />
                    <View style={styles.textFileStyle}>
                        <Text style={styles.textUnoView} numberOfLines={3} adjustsFontSizeToFit minimumFontScale={0.5}>{item.short_description}</Text>
                        {/*} <Text style={styles.textView}>{item.d}</Text>*/}
                        {(item.status == 'unread') ?
                            <Image style={styles.rightImageStyle} source={require('../../assets/images/freccia-trasparente.png')} />
                            :
                            <Image style={styles.rightImageStyle} source={require('../../assets/images/freccia.png')} />
                        }

                    </View>
                </View>
                <View style={styles.viewContainerBottom}>
                    <View style={styles.subViewContainerBottom}>
                        <Text style={styles.bottomText}>Azienda </Text>
                        <Text style={styles.textBottomBold}>
                            {item.companyName} - 
                        </Text>
                        <Text style={styles.bottomText}>
                            Prodotto </Text>
                        <Text style={styles.textBottomBold}>
                            {item.productName}
                            </Text>

                    </View>
                    <Text style={styles.textDate}>{item.created_at}</Text>
                </View>
                <View style={{ width: '100%', height: 2, backgroundColor: '#ffffff', bottom: 0, position: 'absolute' }} />
                <TouchableOpacity style = {styles.cancelTouchable} onPress = {() => this.deleteItem(item)}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.cancelImage}
                            source={require('../../assets/images/freccia-sotto.png')}
                        />
                    </TouchableOpacity>
            </TouchableOpacity>
            </Animated.View>
        );
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <SectionList
                    renderItem={this.renderItem}
                    sections={[
                        { data: this.state.data }
                    ]}
                />
            </View>
        );
    }
};

const styles = {
    containerStyle: {
        width: '100%',
        height: '82.5%'
    },

    globalTouvhableStyle: {
        width: '95%',
        height: 110,
        alignSelf: 'center',
        marginTop: 12,
        //marginRight: '0.5%',
        borderLeftWidth: 4,
        backgroundColor: '#f9fafa',
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOpacity: 0.2,
                shadowOffset: { width: 0, height: 0 },
            },
            android: {
                elevation: 5,
            }
        }),
        alignItems: 'center',
        justifyContent: 'center',

        alignItems: 'flex-start',
    },

    TouchableOpacityStyle: {
        borderLeftColor: '#39b59c',
    },

    TouchableOpacityYellowStyle: {
        borderLeftColor: '#fcc30d',
    },

    TouchableOpacityRedStyle: {
        borderLeftColor: '#ef2e10',
    },

    viewContainerFile: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        aspectRatio: 5,
        elevation: 8,
        marginLeft: '4%',
    },

    fileImageStyle: {
        resizeMode: 'contain',
        height: '85%',
        flex: 0.1
    },

    fileImageYellowStyle: {
        resizeMode: 'contain',
        height: '88%',
        width: '10%'
    },

    fileImageRedStyle: {
        resizeMode: 'contain',
        height: '87%',
        width: '10%'
    },

    textFileStyle: {
        flexDirection: 'row',
        marginLeft: '4%',
        marginTop: 8,
        marginBottom: 8,
        flex: 0.855,
        height: 44,
        backgroundColor: '#ffffff',
        alignItems: 'stretch'
    },

    containerTextStyle: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },

    firstTextStyle: {
        marginLeft: '4%',
        //fontWeight: 'bold',
        fontFamily: 'Montserrat-Bold',
        fontSize: 13
    },

    firstTextStyleSecondPart: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 13,
        marginLeft: 0
    },

    textView: {
        fontFamily: 'Montserrat-Regular',
        marginTop: -5,
        marginLeft: -14,
        marginBottom: '2%'
    },

    textUnoView: {
        height: '100%',
        fontFamily: 'Montserrat-Regular',
        //marginTop: -5,
        marginLeft: 8,
        fontSize: 18,
        flex: 0.8,

    },

    bottomText: {
        fontSize: 10,
        fontFamily: 'Montserrat-Regular',
    },

    rightImageStyle: {
        resizeMode: 'contain',
        flex: 0.2,
        height: 40,
        backgroundColor: '#ffffff',
        marginRight: 8,
        alignSelf: 'center'
    },

    textBottomBold: {
        fontSize: 10,
        fontFamily: 'Montserrat-SemiBold'
    },

    viewContainerBottom: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },

    subViewContainerBottom: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginLeft: '4%',

    },

    textDate: {
        fontSize: 9,
        marginRight: '6%',
        fontFamily: 'Montserrat-Regular',
    },

    cancelImage: {
        height: 25,
        width: 25
    },

    cancelTouchable: {
        position: 'absolute',
        right: 4,
        top: 4,
        height: 40,
        width: 40,
        alignItems: 'flex-end'
    }



};



