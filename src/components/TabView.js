import * as React from 'react';
import { View, StyleSheet, Dimensions, Text, TouchableOpacity,TouchableHighlight, Slider } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';


const initialLayout = {
    height: 0,
    width: Dimensions.get('window').width,
};



class FirstRoute extends React.Component {
    constructor(props) {
        super(props)
        this.state = { clicked: true, marca: true, sameFarmaClass: true }
        this.clickButton = this.clickButton.bind(this)
        this.setParentState = props.callback;
    }

    clickButton(number) {
        if (number === 1) {
            let clicked = this.state.clicked
            this.setState({ clicked: !clicked })
            this.setParentState({clicked: !clicked})
            //filterArray["clicked"] = !clicked;
        } else if (number === 2) {
            let marca = this.state.marca
            this.setParentState({marca: !marca})
            this.setState({ marca: !marca })
        } else if (number === 3) {
            let sameFarmaClass = this.state.sameFarmaClass
            this.setParentState({sameFarmaClass: !sameFarmaClass})
            this.setState({ sameFarmaClass: !sameFarmaClass })
        }
    }

    render() {
        return (
            <View style={styles.filterSection}>
                <View style={styles.viewFilter}>
                    <TouchableOpacity style={styles.touchFilterSelection}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(1)
                        }}>{this.state.equuivalent === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}
                        </TouchableOpacity>
                        <Text style={styles.textFilter}>EQUIVALENTE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchFilterSelection} >
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(2)
                        }}>{this.state.marca === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}</TouchableOpacity>
                        <Text style={styles.textFilter}>MARCA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchFilterSelection}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(3)
                        }}>{this.state.sameFarmaClass === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}</TouchableOpacity>
                        <Text style={styles.textFilter}>STESSA CLASSE FARMACEUTICA</Text>
                    </TouchableOpacity>
                </View>
            </View>)
    }
}
class SecondRoute extends React.Component {
    constructor(props) {
        super(props)
        this.state = { click: true, clicked1: true, clicked2: true, compresse: true, granulato: true, clicked5: true, clicked6: true, clicked7: true }
        this.clickButton = this.clickButton.bind(this),
        this.setParentState = props.callback;
        this.isFilterActive = props.filter;
    }

    clickButton(number) {
        if (number === 1) {
            let click = this.state.click
            this.setParentState({click: !click})
            this.setState({ click: !click })
        } else if (number === 2) {
            let clicked1 = this.state.clicked1
            this.setParentState({clicked1: !clicked1})
            this.setState({ clicked1: !clicked1 })
        } else if (number === 3) {
            let clicked2 = this.state.clicked2
            this.setParentState({clicked2: !clicked2})
            this.setState({ clicked2: !clicked2 })
        }
        else if (number === 4) {
            let compresse = this.state.compresse
            this.setParentState({compresse: !compresse})
            this.setState({ compresse: !compresse })
        }
        else if (number === 5) {
            let granulato = this.state.granulato
            this.setParentState({granulato: !granulato})
            this.setState({ granulato: !granulato })
        }
        else if (number === 6) {
            let clicked5 = this.state.clicked5
            this.setParentState({clicked5: !clicked5})
            this.setState({ clicked5: !clicked5 })
        }
        else if (number === 7) {
            let clicked6 = this.state.clicked6
            this.setParentState({clicked6: !clicked6})
            this.setState({ clicked6: !clicked6 })
        }
        else if (number === 8) {
            let clicked7 = this.state.clicked7
            this.setParentState({clicked7: !clicked7})
            this.setState({ clicked7: !clicked7 })
        }
    }

    render() {
        return (
            <View style={styles.filterSection}>
                <View style={styles.containerSecondView}>
                    <TouchableOpacity style={styles.touchInternalSecondView}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(1)
                        }}>{this.state.click === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}
                        </TouchableOpacity>
                        <Text style={styles.textSecondView}>COMPRESSE E CAPSULE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchInternalSecondView}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(2)
                        }}>{this.state.clicked1 === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}
                        </TouchableOpacity>
                        <Text style={styles.textSecondView}>GRANULATO E POLVERI</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.containerSecondView}>
                    <TouchableOpacity style={styles.touchInternalSecondView}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(3)
                        }}>{this.state.clicked2 === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}
                        </TouchableOpacity>
                        <Text style={styles.textSecondView}>COMPRESSE E CAPSULE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchInternalSecondView}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(4)
                        }}>{this.state.compresse === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}
                        </TouchableOpacity>
                        <Text style={styles.textSecondView}>GRANULATO E POLVERI</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.containerSecondView}>
                    <TouchableOpacity style={styles.touchInternalSecondView}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(5)
                        }}>{this.state.granulato === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}
                        </TouchableOpacity>
                        <Text style={styles.textSecondView}>COMPRESSE E CAPSULE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchInternalSecondView}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(6)
                        }}>{this.state.clicked5 === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}
                        </TouchableOpacity>
                        <Text style={styles.textSecondView}>GRANULATO E POLVERI</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.containerSecondView}>
                    <TouchableOpacity style={styles.touchInternalSecondView}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(7)
                        }}>{this.state.clicked6 === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}
                        </TouchableOpacity>
                        <Text style={styles.textSecondView}>COMPRESSE E CAPSULE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchInternalSecondView}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(8)
                        }}>{this.state.clicked7 === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}
                        </TouchableOpacity>
                        <Text style={styles.textSecondView}>GRANULATO E POLVERI</Text>
                    </TouchableOpacity>
                </View>
            </View>)
    }
}

class ThirdRoute extends React.Component {
    constructor(props) {
        super(props)
        this.state = { lattosio: true, glutine: true, glucosio: true }
        this.clickButton = this.clickButton.bind(this)
        this.setParentState = props.callback;
    }

    clickButton(number) {
        if (number === 1) {
            let lattosio = this.state.lattosio
            this.setParentState({lattosio: !lattosio})
            this.setState({ lattosio: !lattosio })
        } else if (number === 2) {
            let glutine = this.state.glutine
            this.setParentState({glutine: !glutine})
            this.setState({ glutine: !glutine })
        } else if (number === 3) {
            let glucosio = this.state.glucosio
            this.setParentState({glucosio: !glucosio})
            this.setState({ glucosio: !glucosio })
        }
    }

    render() {
        return (
            <View style={styles.filterSection}>
                <View style={styles.viewFilter}>
                    <TouchableOpacity style={styles.touchFilterSelection}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(1)
                        }}>{this.state.lattosio === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}

                        </TouchableOpacity>
                        <Text style={styles.textFilter}>CONTIENE LATTOSIO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchFilterSelection} >
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(2)
                        }}>{this.state.glutine === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}</TouchableOpacity>
                        <Text style={styles.textFilter}>CONTIENE GLUTINE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchFilterSelection}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.clickButton(3)
                        }}>{this.state.glucosio === true && <View style={{
                            width: 10,
                            height: 10,
                            backgroundColor: '#ffffff',
                            borderRadius: 7.5,
                        }}></View>}</TouchableOpacity>
                        <Text style={styles.textFilter}>CONTIENE GLUCOSIO</Text>
                    </TouchableOpacity>
                </View>
            </View>)
    }
}
class FourthRoute extends React.Component {

    constructor(props) {
        super(props)
        this.state = { price: 0 }
    }

    change(price) {
        this.setState(() => {
            return {
                price: parseFloat(price),
            };
        });
    }

    render() {
        return (
            <View style={styles.filterSection}>
                <View style={{ alignItems: 'center', marginTop: '25%' }}>
                    <Text style={styles.textPrice}>{String(this.state.price)}</Text>
                    <Slider
                        style={{ width: 300 }}
                        step={0.5}
                        minimumValue={0}
                        maximumValue={387}
                        value={this.state.price}
                        onValueChange={this.change.bind(this)}
                        thumbTintColor={'#25A588'}
                    />
                </View>
            </View>
        )
    }
}



export default class TabView extends React.Component {


    constructor(props) {
        super(props);

    this.state = {
        index: 0,
        routes: [
            { key: 'first', title: 'FARMACO' },
            { key: 'second', title: 'TIPOLOGIA' },
            { key: 'third', title: 'INTOLLERANZE' },
            { key: 'fourth', title: 'PREZZO' }
        ],
        
        /*FirstTab*/
        equuivalent:true, 
        marca: true, 
        sameFarmaClass: true,
        /*SecondTab*/
        click: true, 
        clicked1: true, 
        clicked2: true, 
        compresse: true, 
        granulato: true, 
        clicked5:true, 
        clicked6: true, 
        clicked7: true,
        /*ThirdTab*/
        lattosio: true, 
        glutine: true, 
        glucosio: true
       
    };

    this.callback = props.filter

    this.setParentState = this.setParentState.bind(this);
    this.saveFilter = props.filterCallback;
    this.saveFilter=this.saveFilter.bind(this)
}


saveFilter() {

    var filterArray = [this.state.clicked, this.state.marca, this.state.sameFarmaClass, this.click]
    this.callback(filterArray)
}


    renderLabel = ({ route }) => {
        return (
            <View>
                <Text style={styles.titleText}>{route.title}</Text>
            </View>
        )
    }


    setParentState(keyAndValue) {
        this.setState(keyAndValue);
        this.isFilterActive(keyAndValue)
    }

    sendResearch() {
        alert('ciao')
    }

    _handleIndexChange = index => {
        this.setState({ index })

    };

    _renderHeader = (props) =>
        <TabBar
            {...props}
            tabStyle={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}
            renderLabel={this.renderLabel}
            onIndexChange={this._handleIndexChange}
            style={{ backgroundColor: '#EDEDED', height: 50, justifyContent: 'center' }}
            indicatorStyle={{
                borderBottomWidth: 3,
                borderBottomColor: '#25A588',
                width: '13%',
                marginLeft: '4%',
                marginRight: '2%'
            }}


        />;

    _renderScene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return <FirstRoute callback={this.setParentState} filter={this.isFilterActive}/>;
            case 'second':
                return <SecondRoute callback={this.setParentState} />;
            case 'third':
                return <ThirdRoute callback={this.setParentState} />;
            case 'fourth':
                return <FourthRoute callback={this.setParentState} />;

            default:
                return null;
        }
    };

    render() {
        return (
            <View  style={{ height: '100%' }}>
                  <View style={{ height: '100%' }}>
            <TabViewAnimated
                navigationState={this.state}
                renderLabel={this.renderLabel}
                renderScene={this._renderScene}
                renderHeader={this._renderHeader}
                onIndexChange={this._handleIndexChange}
                initialLayout={initialLayout}
            />
            <View style={styles.saveButtonStyle}>
            <TouchableHighlight style={styles.orangeButton} onPress={() => {
                    sendResearch()
                    }}>
                        <Text style={{ color: '#ffffff', fontFamily: 'Montserrat-Bold'}}>Cerca</Text>
                    </TouchableHighlight>
            </View>
            </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    viewFilter: {
        marginTop: '20%',
        backgroundColor: '#ffffff',
        alignItems: 'center',
        height: '50%'
    }, touchFilterSelection: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '5%',
        width: '80%',
        borderBottomWidth: 1,
        borderBottomColor: '#dcdcdc'
    },

    buttonStyle: {
        width: 20,
        height: 20,
        backgroundColor: '#25A588',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '5%'
    },
    textFilter: {
        marginLeft: '5%',
        width: '50%',
        marginBottom: '5%',
        color: '#6E7070'
    },

    filterSection: {
        height: '100%',
        backgroundColor: '#ffffff'
    },

    containerSecondView: {
        flexDirection: 'row',
        height: '23%',
        width: '100%',
    },

    touchInternalSecondView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '5%',
        marginLeft: '2%',
        marginRight: '2%',
        width: '47%',
        borderBottomWidth: 1,
        borderBottomColor: '#dcdcdc'
    },

    textSecondView: {
        marginLeft: '5%',
        width: '50%',
        marginBottom: '2%',
        color: '#6E7070'
    },
    textPrice: {
        fontSize: 50,
        color: '#25A588',
        marginBottom: '10%'
    },
    titleText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 9,
        color: '#6E7070'
    },
    orangeButton: {
        width: '80%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e94e1b',
        padding: 10,
        marginTop: '3%',
        borderRadius: 25
    },
    saveButtonStyle: {
        height: '15%',
        alignItems: 'center',
        backgroundColor: '#ffffff'
    }
});