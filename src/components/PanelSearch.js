import React from 'react';
import { Component, StyleSheet, Text, View, Image, TouchableHighlight, Animated, TextInput } from 'react-native'; //Step 1


class Panel extends React.Component {
    constructor(props) {
        super(props);

        this.icons = {    
            'down': require('../../assets/images/close_icon.png')
        };

        this.state = {       
            expanded: false,
            animation: new Animated.Value(),
            searchDelegate: props.searchCallback, 
            closeDelegate: props.closeCallback,
            data:''
        };

        this.toggle = this.toggle.bind(this);
    }

    componentWillMount() {
    }

    componentDidMount() {

        this.toggle();
    }

    toggle() {
        

        let initialValue = this.state.expanded ? 150 : 60;
        let finalValue = this.state.expanded ? this.close() : 150;

            
        this.setState(()=>{
            expanded: false 
        });

        this.state.animation.setValue(initialValue);  
        Animated.timing(    
            this.state.animation,
            {
                toValue: finalValue,
                duration: 300
            }
        ).start();  

        
    }



    render() {
        let icon = this.icons['down'];
        return (
            <View>
            <Animated.View style={[styles.mainContainer, { height: this.state.animation }]}>
                <View>
                    <TouchableHighlight
                        style={styles.button}
                        onPress={this.state.closeDelegate.bind(this)}
                        underlayColor="#f1f1f1">
                        <View style={styles.container} >
                            <View style={styles.titleContainer} >
                              
                                <Image
                                    style={styles.buttonImage}
                                    source={icon}
                                ></Image>
                            </View>

                            <View style={styles.body}>
                                <TextInput
                            style={{ width: '80%', color: '#ffffff', fontFamily: 'Montserrat-Regular' }}
                            underlineColorAndroid={'#ffffff'}
                            placeholder={'Inserisci chiave di ricerca'}
                            placeholderTextColor={'#75c2ae'}
                            onChangeText={(text) => {
                                let testo = this.state.text
                                let data = this.state.data
                                
                                fetch('http://dotto.bincode.it/api/products/search?q=' + this.state.text, {
                                    method: 'GET'
                                })
                                    .then((response) => response.json())
                                    .then((responseJson) => {


                                        this.setState({ data: responseJson })
                                        


                                    })
                                this.setState({ text })
                            
                            }
                            }
                            value={this.state.text}
                        />

                            </View>
                        </View>
                    </TouchableHighlight>
                </View>
            </Animated.View>
            <View style={styles.menuToolbar}>
                        <TouchableHighlight>
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 10 }}>CRONOLOGIA</Text>
                        </TouchableHighlight>
                        <TouchableHighlight>
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 10 }}>PREFERITI</Text>
                        </TouchableHighlight>
                        <TouchableHighlight>
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 10 }}>FILTRI</Text>
                        </TouchableHighlight>
                    </View>
            
         </View>
        );
    }
}

var styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#25A588',
        height: 30
    },
    container: {
        backgroundColor: '#25A588',
        //overflow: 'hidden'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: '5%',
    },
    title: {
       
        color: '#ffffff',
        fontFamily: 'Montserrat-Regular'
    },
    menuToolbar: {
        position: 'relative',
        bottom: 0,
        width: '100%',
        flexDirection: 'row', 
        justifyContent: 'space-around', 
        alignItems: 'center', 
        backgroundColor: '#ffffff', 
        elevation: 10, 
        height: 45
    },
    button: {
      
    },
    buttonImage: {
        resizeMode: 'contain',
        backgroundColor:'#25A588',
        marginTop: '1%',
        width: 20,
        //height: 40,
        marginLeft:'70%'
        //marginRight: '10%',
    },
    body: {
        padding: 10,
        paddingTop: 0,
        width:'100%'
       
    }
});
export default Panel;