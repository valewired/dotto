import React from 'react';
import {View, Text} from 'react-native';
import Image from 'react-native-remote-svg';
import Panel from '../components/Panel'; 


const BarFooter=()=>{
    return (
        <View style={styles.containerStyle}>
            <View style={styles.touchableOpacityStyle}>
            <Panel title="Farmaci con lo stesso  PRINCIPIO ATTIVO">
               <Text style={styles.textNormal}>
               Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
               </Text>
               </Panel>
               <View style={styles.viewStyleBottom}>
               <Image style={styles.imageBottomStyle}  source={require('../../assets/images/solo-freccia.png')} /> 
               </View>
            </View>
            <View style={styles.touchableOpacityStyle}>
            <Panel title="Farmaci con lo stesso  PRINCIPIO ATTIVO">
               <Text style={styles.textNormal}>
               Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
               </Text>
               </Panel>
               <View style={styles.viewStyleBottom}>
               <Image style={styles.imageBottomStyle}  source={require('../../assets/images/solo-freccia.png')} /> 
               </View>
            </View>
            <View style={{height:100}}>
            <View style={styles.touchableWhiteOpacityStyle}>
             <View style={styles.viewText}>
               <Text  style={styles.viewTextStyle}>SUBCUVIA</Text>
               <Text  style={styles.viewTextLittleStyle}>SHIRE ITALIA</Text>
               </View>
               <Image style={styles.iconStyle}  source={require('../../assets/images/freccia-sotto.png')}/> 
            </View>
            <View style={styles.touchableWhiteOpacityStyle}>
            <View style={styles.viewText}>
            <Text  style={styles.viewTextStyle}>SUBCUVIA</Text>
               <Text  style={styles.viewTextLittleStyle}>SHIRE ITALIA</Text>
               </View>
               <Image style={styles.iconStyle}  source={require('../../assets/images/freccia-sotto.png')}/> 
            </View>
            <View style={styles.touchableWhiteOpacityStyle}>
            <View style={styles.viewText}>
            <Text  style={styles.viewTextStyle}>SUBCUVIA</Text>
               <Text  style={styles.viewTextLittleStyle}>SHIRE ITALIA</Text>
               </View>
               <Image style={styles.iconStyle}  source={require('../../assets/images/freccia-sotto.png')}/> 
            </View>
            </View>
            <View style={styles.touchableOpacityBottomStyle}>
               <Text style={styles.textNormal}>
                   Farmaci con lo stesso {'\t'}
                   <Text style={styles.textStyle}>
                       PRINCIPIO ATTIVO
                       </Text>
               </Text>
               <View style={styles.viewStyleBottom}>
               <Image  style={styles.imageBottomStyle} source={require('../../assets/images/solo-freccia.png')} /> 
               </View>
            </View>
            <View style={styles.touchableOpacityBottomStyle}>
               <Text style={styles.textNormal}>
                   Farmaci con lo stesso {'\t'}
                   <Text style={styles.textStyle}>
                       PRINCIPIO ATTIVO
                       </Text>
               </Text>
               <View style={styles.viewStyleBottom}>
               <Image  style={styles.imageBottomStyle} source={require('../../assets/images/solo-freccia.png')} /> 
               </View>
            </View>
        </View>
    )
};


const styles={
    containerStyle: {
        width: '100%',
        height:200,
    },

    touchableOpacityStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#e5e5e5',
        width:'100%',
        height:200
    },

    touchableOpacityBottomStyle: {
        flexDirection: 'row',
        //justifyContent: 'space-around'
        backgroundColor: '#e5e5e5',
        width:'100%',
        height:50,
        alignItems:'flex-start',
        justifyContent:'space-between'
    },

    touchableWhiteOpacityStyle: {
        flexDirection: 'row',
        justifyContent:'space-between',
        backgroundColor: '#f8f8f8',
        width:'100%',
        height:100,
        borderBottomWidth:1,
        borderBottomColor: '#bdb8b8',  
    },

    viewStyleBottom: {
        //aspectRatio: 10,
        //marginLeft:'20%',
        marginTop:'2%',
        height: 100,
        width:'15%',
        alignItems:'center',
        justifyContent:'center'
    },

    imageBottomStyle: {
        height:30,
        width:'30%'
    },

    imageStyle: {
        
            resizeMode: 'contain',
            //aspectRatio: 10,
            marginLeft:'14%',
            marginTop:'2.5%',
            height: 30,
            width:'20%',
    
    },

    textStyle: {
        fontFamily: 'Montserrat-Bold',
        marginTop:'3%'
    },

    textNormal: {
        fontFamily: 'Montserrat-Regular',
        marginLeft: '4%',
        marginTop: '2%'
    },

    viewText: {
        marginLeft: '8%',
        justifyContent:'center',
        width: '80%',
        height:70

    },

    viewTextLittleStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 7,
        color: '#d4cece'
    },

    viewTextStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
        color: '#7c7c7c',
        marginTop:'3%'
    },

    iconStyle: {
        resizeMode: 'contain',
        marginTop:'2%',
        width: '10%',
        height:50,
        marginRight: '5%',
       // backgroundColor:'green'
    }
}





export default BarFooter;