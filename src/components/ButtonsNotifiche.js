import React from 'react';
import { View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-remote-svg';

export default class ButtonsNotifiche extends React.Component {
    render() {
        return (
            <View style={styles.containerStyle}>
                <TouchableOpacity style={styles.touchableFirstStyle}>
                    <Text style={styles.textTouchableButton}>STRUMENTI</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.touchableStyle}>
                    <Text style={styles.textTouchableButton}>NOTIFICHE</Text>

                    <Image style={styles.iconButtonStyle} source={require('../../assets/images/icon-359.png')} />

                </TouchableOpacity>

            </View>

        );
    }
};

const styles = {
    containerStyle: {
        backgroundColor: '#ffffff',
        width: '100%',
        aspectRatio: 9,
        flexDirection: 'row',
    },

    touchableFirstStyle: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },

    touchableStyle: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        borderBottomWidth: 3,
        borderBottomColor: '#25A588',
    },

    textTouchableButton: {
        fontSize: 12,
        color: '#3a3a39',
        fontFamily: 'Montserrat-Regular'
    },

    iconButtonStyle: {
        //resizeMode: 'contain',
        marginLeft: '1%',
        height: 20,
        width: 40,
    }
};

