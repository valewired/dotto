import React from 'react';
import { SectionList, View, Text, TouchableOpacity, Header } from 'react-native';
import Image from 'react-native-remote-svg';
import { Dimensions } from 'react-native';

export default class CentralBox extends React.Component {

    renderItem = ({ item }) => {
        return (
            <View>
                <TouchableOpacity style={styles.TouchableOpacityTopStyle}>
                 <View>
                    <Text style={styles.titleTextTouchableOpacityStyle}>{item.a}</Text>
                    <Text style={styles.secondTitleTextTouchableOpacityStyle}>{item.b}</Text>
                </View>
             <Image style={styles.imageStyle} source={require('../../assets/images/freccia-grigia.png')} />
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <SectionList
                     keyExtractor={(item, index) => item.id}
                    renderItem={this.renderItem}
                    sections={[
                        { data: [{a:'Suncvia', b:'Shire spa', id:0},{a:'Suncvia', b:'Shire spa',id:1},{a:'Suncvia', b:'Shire spa',id:2},{a:'Suncvia', b:'Shire spa',id:3},{a:'Suncvia', b:'Shire spa' ,id:4} , {a:'Suncvia', b:'Shire spa', id:5},{a:'Suncvia', b:'Shire spa',id:6}] }

                     ]}
                />
                <View style={styles.viewContainerRightStyle}>
                    <TouchableOpacity style={styles.TouchableOpacityRight}>
                        <Image style={styles.imageRightStyle} source={require('../../assets/images/user.png')} />
                        <Text style={styles.textBigRightStyle}>OOPS!</Text>
                        <Text style={styles.textRightStyle}>CRONOLOGIA VUOTA</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
};

var { height, width } = Dimensions.get('window')

const styles = {
    containerStyle: {
        width: '100%',
        height: '43%',
        flexDirection: 'row',
        backgroundColor: '#ffffff'
    },
    viewContainerRightStyle: {
        backgroundColor: '#ffffff',
        width: '50%',
        height:'100%',
        //borderRightWidth: 1,
        borderLeftColor: '#dcdcdc',
        borderLeftWidth: 1,
        flexDirection: 'column',
        justifyContent: 'space-around',
    },


    titleTextTouchableOpacityStyle: {
        color: '#99969b',
        fontSize: 15,
        fontFamily: 'Montserrat-Regular',
        marginTop:'2%'
        //paddingLeft: '5%',
    },

    secondTitleTextTouchableOpacityStyle: {
        color: '#99969b',
        fontSize: 8,
        fontFamily: 'Montserrat-Regular',
        paddingBottom:'10%'
    },
    textTouchableOpacity: {
        fontFamily: 'Montserrat-Regular',
        color: '#99969b',
        fontSize: 8,
        paddingBottom: '10%',
        paddingLeft: '9%'
    },

    TouchableOpacityTopStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: '5%',
        //marginTop: '3%',
        //marginBottom: '1%',
        width: '90%',
        borderBottomColor: '#dcdcdc',
        borderBottomWidth: 1,
    },

    TouchableOpacityStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: '5%',
        marginTop: '2%',
        borderBottomColor: '#dcdcdc',
        borderBottomWidth: 1,
        width: '83%',
        height: '22%',
    },

    viewTouchableOpacityStyle: {
        marginLeft: '10%',
    },

    viewImageStyle: {
        marginTop: '4%',
        marginLeft: '20%',
        alignItems: 'flex-end',
        height: '45%',
        width: '45%',
    },

    imageStyle: {
        marginTop:'5%',
        marginRight: -5,
        resizeMode: 'contain',
        width: '20%',
        height: '50%',
    },

    TouchableOpacityRight: {
        height: '100%',
        alignItems: 'center',
        
    },

    imageRightStyle: {
        marginTop: '15%',
        marginBottom: '10%',
        height: '38%',
        width: '40.5%',
       


    },

    textRightStyle: {
        fontFamily: 'Montserrat-Regular',
        color: '#99969b',
        fontSize: 10
    },

    textBigRightStyle: {
        fontFamily: 'Montserrat-Regular',
        color: '#99969b',
        fontSize: 15
    }
};
