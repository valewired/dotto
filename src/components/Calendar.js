import React from 'react';
import { View, Text, BackAndroid } from 'react-native';
import Image from 'react-native-remote-svg';

const Calendar= () =>{
    
    const {viewS, imgS ,textS,testoStyle}= styles;
    
    return(
      <View style={viewS}>
          <Image style={imgS} source={require('../../assets/images/calendar.png')} />
          <Text style={textS}>Roma-Palazzo dei Congressi</Text>
          <Text style={testoStyle}>Venerdi 18 Marzo 2018</Text>
      </View>
    );
};


const styles={
    viewS:{
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginLeft:'3%',
        marginRight:'3%', 
        width:'94%',
        aspectRatio: 7.5,
        borderLeftColor: '#D3D2D2',
        borderLeftWidth:1,
        borderRightColor: '#D3D2D2',
        borderRightWidth:1,
        backgroundColor:'#ffffff'
    },
    imgS: {
        resizeMode: 'contain',
        marginLeft: '4%',
        height: 30,
        width: 35        
    },

    textS: {
        alignItems: 'center',
        marginLeft: '18%',
        marginTop: -30,
        fontSize: 10,
        fontFamily: 'Montserrat-Regular'
    },

    testoStyle: {
        marginLeft: '18%',
        fontFamily: 'Montserrat-Bold'
    }
}

export default Calendar 
