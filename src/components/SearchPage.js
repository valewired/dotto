import React from 'react';
import { View, SectionList,Text } from 'react-native';

export default class SearchPage extends React.Component {

   renderItem=({item}) => {
         return (
            <View style={styles.viewList}>
                <Text style={styles.titleTextList}>{item.a}</Text>
                <Text style={styles.textList}>{item.b} </Text>
            </View>
         )
     };



    render() {

        return (
            <View>
                <View>
                    <Text style={{fontFamily:'Montserrat-Bold', color: '#25A588', fontSize:25, marginLeft:'3%'}}>A</Text>
                </View>
                <SectionList

                    renderItem={this.renderItem}
                    sections={[
                        {
                            data: [{ a: 'A-a O2 Gradient', b:'Asset mutiple congurent salisburho granella ulbert' , key: 0 },
                            { a: 'Abbey', b:'Asset mutiple congurent salisburho granella ulbert', key: 1 },
                            { a: 'ABC',b:'Asset mutiple congurent salisburho granella ulbert', key: 2 },
                            ]
                        }
                    ]}
                    />
            </View>
            )
        }
    }

const styles={
    viewList:{
        marginTop:'3%',
        borderBottomColor: '#dcdcdc',
        borderBottomWidth: 1,
    },

    textList: {
        marginLeft:'3%',
        fontFamily:'Montserrat-Regular',
        fontSize:13,
        color: 'grey',
        marginBottom: '4%'
    },

    titleTextList: {
        marginLeft:'3%',
        fontFamily:'Montserrat-Bold',
        color: '#25A588',
        fontSize:15
    }
}
