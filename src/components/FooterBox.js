import React from 'react';
import { View, TouchableOpacity, Text, Dimensions } from 'react-native';
import Image from 'react-native-remote-svg';
//import CustomIcon from '../../CustomIcon';

export default class FooterBox extends React.Component {

    constructor(props) {
        super(props);

        this.footerDelegate = props.pressCallback;
    }

    render(){
        return (
            <View style={styles.containerStyle}>
                <TouchableOpacity style={styles.TouchableOpacityLeftRight} onPress={() => { this.footerDelegate(3) }}>
                <View style={styles.viewIconStyle}>
                    <Image style={styles.iconStyle} source={require('../../assets/images/icon-359.png')} />
                    </View>
                    <View style={styles.viewImage}>
                    <Image style={styles.imageStyle} source={require('../../assets/images/icon-informatori.png')} />
                    </View>
                    <Text style={styles.textStyle}>Informatori</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.TouchableOpacityCenter}  onPress={() => { this.footerDelegate(0) }}>
                    <Image />
                    <View style={styles.viewImageC}>
                        <Image style={styles.imageStyle} source={require('../../assets/images/icon-documenti.png')} />
                    </View>
                    <Text style={styles.textStyleB}>documenti</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.TouchableOpacityCenter}   onPress={() => { this.footerDelegate(2) }}>
                    <Image />
                    <View style={styles.viewImageC}>
                        <Image style={styles.imageStyle} source={require('../../assets/images/icon-calcolatore.png')} />
                    </View>
                    <Text style={styles.textStyleB}>eventi</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.TouchableOpacityLeftRight}  onPress={() => { this.footerDelegate(1) }}>
                    <Image />
                    <View style={styles.viewImageB}>
                        <Image style={styles.imageStyle} source={require('../../assets/images/icon-sitoweb.png')} />
                    </View>
                    <Text style={styles.textStyleB}>siti web</Text>
                </TouchableOpacity>
            </View>
        );
}
};


const window= Dimensions.get('window');

const styles = {
    containerStyle: {
        width: '100%',
        height:'14.5%',
        flexDirection: 'row',
        position: 'absolute',
        bottom:0,
        backgroundColor:'#ffffff',
        borderTopWidth: 1,
        borderTopColor: '#dcdcdc',
    },

    TouchableOpacityLeftRight: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '28%'
    },

    imageStyle: {
        resizeMode: 'contain',
        height: '60%',
        width: '70%',
    },

    viewImage: {
        marginTop:-10,
        width: '100%',
        height: '60%',
        borderRightWidth: 1,
        borderRightColor: '#dcdcdc',
        justifyContent: 'center',
        alignItems: 'center'
    },

    viewImageB: {
        marginTop: '10.5%',
        width: '100%',
        height: '60%',
        borderRightWidth: 1,
        borderRightColor: '#dcdcdc',
        justifyContent: 'center',
        alignItems: 'center',
    },
 

    viewImageC: {
    marginTop: '12%',
    width: '100%',
    height: '60%',
    borderRightWidth: 1,
    borderRightColor: '#dcdcdc',
    justifyContent: 'center',
    alignItems: 'center',
},

    TouchableOpacityCenter: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '22%'
    },

    textStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 10
    },

    textStyleB: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 10
    },

    iconStyle: {
        height: '90%',
        width: '100%'
    },

    viewIconStyle:{
        //marginTop:'2%',
        height: '17%',
        width: '35%',
        marginBottom:'2%'
    }

};

