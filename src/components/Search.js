import React from 'react';
import { Modal, View, Image, Text, TouchableOpacity, TextInput, TouchableHighlight, KeyboardAvoidingView, Switch, Animated, SectionList, ActivityIndicator } from 'react-native';
import EventRegister from './EventRegister';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import DefaultPreference from 'react-native-default-preference';
import TabView from './TabView';



export default class Search extends React.Component {


    constructor(props) {
        super(props);

        this.icons = {
            'down': require('../../assets/images/close_icon.png')
        };

        this.state = {
            expanded: false,
            animation: new Animated.Value(),
            searchDelegate: props.searchCallback,
            data: [],
            isLoading: false,
            latitude: null,
            longitude: null,
            error: null,
            filterShown: false,
            filterActive: false,
            filterStatus: false,
            typeButton: 'farmaco',
            selectButton: 'farmaco',
        };

        this.filterKeys = ["banane", "carciofi", "compresse", "granulato"]
        this.filterData = [true, true, true, true, true, true, true, true, true, true, true, true, true, true]

        this.closeDelegate = props.closeCallback;
        this.toggle = this.toggle.bind(this);
        this.sendSearchRequest = this.sendSearchRequest.bind(this);
        this.showFilter = this.showFilter.bind(this)
        this.TouchableOpacityStyle = this.TouchableOpacityStyle.bind(this)
        this.setFilterValues=this.setFilterValues.bind(this)
    }

    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => {
                this.setState({ error: error.message })
                alert(error.message)
            });

    }

    componentDidMount() {


        this.toggle();
    }

    toggle() {
        let initialValue = this.state.expanded ? 150 : 60;
        let finalValue = this.state.expanded ? 60 : 150;

        this.setState({
            expanded: !this.state.expanded
        });

        var animationTime = (this.state.expanded ? 300 : 300)

        this.state.animation.setValue(initialValue);
        Animated.timing(
            this.state.animation,
            {
                toValue: finalValue,
                duration: animationTime
            }
        ).start();


    }

    _setMaxHeight(event) {
        this.setState({
            maxHeight: event.nativeEvent.layout.height
        });

    }

    _setMinHeight(event) {
        this.setState({
            minHeight: event.nativeEvent.layout.height
        });

    }

    renderItem = ({ item }) => {

        return (
            <TouchableHighlight onPress={() => {
                this.closeDelegate();
                var recipe = EventRegister.getInstance()
                recipe.trackEvent(item.name, 'view', item.id, this.state.latitude, this.state.longitude)
                this.props.navigation.navigate('RecipeDetail', params = { url: item.url })
            }}>
                <View style={styles.productRow}>
                    <Text id={item.id} url={item.url} style={styles.productTitle}>{item.name}</Text>
                    <Image source={require('../../assets/images/star_empty.png')} style={styles.favoritesIcon} />
                </View>
            </TouchableHighlight>

        )
    }

    sendSearchRequest(queryString) {
        //Add filter values
        this.filterData.forEach( (currentValue, index) => {
            if(!currentValue)
                queryString += "&filter[" + this.filterKeys[index]  + "]=false"
        })


        //Start loading
        this.setState({
            isLoading: true
        })
        fetch('http://dotto.bincode.it/api/products/search?q=' + queryString, {
            method: 'GET',
            headers: new Headers({
                "Authorization": `Bearer ${global.userToken}`,
                'Accept': 'multipart/form-data',
                'Content-Type': 'multipart/form-data'
            })
        })
            .then((response) => response.json()).then((responseJson) => {
                this.setState({ data: responseJson.items })
                //Stop loading
                this.setState({
                    isLoading: false
                })
            })
    }


    

    showFilter() {
      
        this.setState({ filterStatus: true })
    }

    setFilterValues(filterArr) {

        this.filterData = filterArr

        // for(....) { iterate over each filter setting
        //     if setting[i] == false
        //         return true; //Just one setting equal to off to activate filter

        // }

        return false;
    }

    TouchableOpacityStyle(pressButton) {

        if (this.state.typeButton === pressButton) {
            return {
                borderBottomColor: '#25A588',
                alignItems: 'center',
                justifyContent: 'center',
                height: 48,
                width: '20%',
                borderBottomWidth: 3

            }
        }
        else {
            return {
                borderBottomWidth: 3,
                borderBottomColor: '#ededed'
            }

        }
    }

    SelectButtonFilter(selectButton) {

        let typeButton = this.state.typeButton
        this.setState({ typeButton: selectButton })
        this.TouchableOpacityStyle(selectButton)
    }

    render() {
        data = this.state.data
        let icon = this.icons['down'];

        return (
            <View style={{ flex: 1, ...ifIphoneX({ marginTop: 20 }) }}>
                <Animated.View style={[styles.mainContainer, { height: this.state.animation }]}>
                    <View>
                        <View style={styles.container} >
                            <TouchableOpacity
                                //style={styles.button}
                                onPress={() => {
                                    this.toggle();
                                    setTimeout(this.closeDelegate, 300);
                                }}
                            >
                                <View style={styles.titleContainer} >
                                    <Image style={styles.buttonImage} source={icon} />
                                </View>
                            </TouchableOpacity>
                            <View style={styles.body}>
                                <TextInput
                                    style={styles.searchTextInput}
                                    underlineColorAndroid={'#ffffff'}
                                    placeholder={'Inserisci chiave di ricerca'}
                                    placeholderTextColor={'#75c2ae'}
                                    onChangeText={(text) => {
                                        let data = this.state.data
                                        if (text.length >= 2) {
                                            this.sendSearchRequest(text);
                                            this.setState({ text })

                                        } else {

                                            this.setState({ text })
                                        }
                                    }}
                                    value={this.state.text}
                                />

                            </View>
                        </View>
                    </View>
                </Animated.View>
                <View style={styles.menuToolbar}>
                    <TouchableOpacity style={styles.styleCronologia}>
                        <Image style={styles.imageStyle} source={require('../../assets/images/cronologia_icon.png')} />
                        <Text style={styles.hystoryStyle}>CRONOLOGIA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.styleCronologia}>
                        <Image style={styles.imageStyle} source={require('../../assets/images/star_empty.png')} />
                        <Text style={styles.hystoryStyle}>PREFERITI</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.styleCronologia}>
                        <Text style={styles.hystoryStyle}>FILTRI</Text>
                       
                        <Switch
                            onValueChange={(value) => {
                                if (this.state.filterShown == false) {
                                    this.showFilter();
                                }
                            }}
                            value={this.isFilterActive()}
                            style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }] }}
                        />
                      
                    </TouchableOpacity>
                </View>
                {this.state.filterStatus === true && <View style={{ height: '70%' }}><TabView filterCallback={this.setFilterValues} /></View>}
                <View style={styles.listContainer}>
                    <SectionList
                        style={styles.productsList}
                        contentContainerStyle={{ justifyContent: 'center' }}
                        keyExtractor={(item, index) => item.id}
                        renderItem={this.renderItem}
                        sections={[{ title: 'Risultati', data: this.state.data }]}
                        ListHeaderComponent={() =>
                            (!this.state.data.length ? (this.state.isLoading ? <ActivityIndicator style={styles.loadingIndicator} size="small" color="#ffffff" /> : <Text style={styles.emptyListMessage}>Nessun risultato</Text>) : null)}
                    />
                </View>
            </View>
        );
    }
};


const styles = {
    containerStyle: {
        backgroundColor: '#25A588',
        width: '100%',
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 0

    },
    logoContainerStyle: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center'

    },
    logoStyle: {
        resizeMode: 'contain',
        width: '60%',
        height: '50%'
    },

    iconMenuContainerStyle: {
        width: '25%',
        height: '90%',
        justifyContent: 'center',
        alignItems: 'flex-start',

    },

    iconSearchContainerStyle: {
        width: '25%',
        justifyContent: 'center',
        alignItems: 'flex-end',

    },

    iconStyle: {
        resizeMode: 'contain',
        width: '40%',
        height: '40%'

    },
    iconCancelContainerStyle: {
        backgroundColor: '#25A588', height: '16%', width: '100%', elevation: 5

    },

    mainContainer: {
        backgroundColor: '#25A588',
        height: 30
    },
    container: {
        backgroundColor: '#25A588',
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: '5%',
    },
    title: {

        color: '#ffffff',
        fontFamily: 'Montserrat-Regular'
    },
    menuToolbar: {
        position: 'relative',
        bottom: 0,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        height: 50,
    },
    buttonImage: {
        resizeMode: 'contain',
        backgroundColor: '#25A588',
        marginTop: '1%',
        width: 17,
        marginLeft: '88%'
    },
    body: {
        alignItems: 'center',
        paddingTop: 20,
        width: '100%'

    },
    imageStyle: {
        height: 25,
        width: 25
    },

    listContainer: {
        flex: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
        paddingLeft: 0
    },
    productsList: {
        height: '100%',
        width: '100%',
        backgroundColor: 'transparent'
    },
    emptyListMessage: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: '7%'
    },
    loadingIndicator: {
        marginTop: '7%'
    },
    productRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 50,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#ede9e9'
    },
    productTitle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
        color: '#727474',
        paddingLeft: '10%',

    },
    favoritesIcon: {
        height: 30,
        width: '20%',
        resizeMode: 'contain'
    },

    styleCronologia: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    FilterToolbar: {
        position: 'relative',
        bottom: 0,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#ededed',
        height: 48,
        zIndex: 1
    },
    textFilterSelection: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 10,
        color: '#c0c0c0',

    },
    touchFilterSelection: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '5%',
        width: '80%',
        borderBottomWidth: 1,
        borderBottomColor: '#dcdcdc'
    },

    buttonStyle: {
        width: 30,
        height: 30,
        backgroundColor: '#25A588',
        borderRadius: 15,
    },
    viewFilter: {
        marginTop: '20%',
        backgroundColor: '#ffffff',
        alignItems: 'center',
        //height:'50%'
    },

    filterSection: {
        height: '70%',
        backgroundColor: '#ffffff'
    },

    textFilter: {
        marginLeft: '5%',
        width: '50%'
    },

    hystoryStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 10,
        marginLeft: '2%',
        color: '#6E7070'
    },
    searchTextInput: {
        width: '80%',
        color: '#ffffff',
        fontFamily: 'Montserrat-Regular'
    }
};