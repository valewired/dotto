import React from 'react'
import { View, TextInput, Text, StyleSheet, Dimensions, SectionList, ActivityIndicator, TouchableHighlight, Image, TouchableOpacity } from 'react-native'
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';

const initialLayout = {
    height: 0,
    width: Dimensions.get('window').width,
};

var isFirst = true

class FirstRoute extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchText: "",
            filteredData: [],
            isLoading: false,
            description: undefined,
            secondItem: undefined
        }
    }

    sendSearchRequest(queryString) {
        //Start loading

        this.setState({
            isLoading: true
        })
        var type = 'products'
        if (this.props.indexValue == 1) {
            type = 'active-principles'
        }
        fetch('http://dotto.bincode.it/api/' + type + '/search?q=' + queryString, {
            method: 'GET',
            headers: new Headers({
                "Authorization": `Bearer ${global.userToken}`
            })
        })
            .then((response) => response.json()).then((responseJson) => {
                this.setState({ filteredData: responseJson.items, isLoading: false })
            })
    }


    setMaxHeight(event) {
       
        this.setHeight(event.nativeEvent.layout.height)
    }


    setHeight(height) {
        this.props.setHeight(height)
    }

    render() {
        var type = 'farmaco'
        if (this.props.indexValue == 1) {
            type = 'principio'
        }
        if (this.state.description == undefined) {
            this.setHeight(400)
            return (
                <View style={{ flex: 1 }}>
                    <TextInput
                        style={styles.textInput}
                        placeholder={"Inserisci il nome del" + type}
                        underlineColorAndroid={'rgba(0, 0, 0, 0)'}
                        onChangeText={(text) => {
                            //let testo = this.state.text
                            this.sendSearchRequest(text);
                            this.setState({ searchText: text })
                        }}
                        value={this.state.searchText}
                    />
                    <SectionList
                        style={styles.productsList}
                        contentContainerStyle={{ justifyContent: 'center' }}
                        keyExtractor={(item, index) => item.id}
                        renderItem={this.renderItem}
                        sections={[{ title: 'Risultati', data: this.state.filteredData }]}
                        ListHeaderComponent={() => 
                            (!this.state.filteredData.length ?  (this.state.isLoading ? <ActivityIndicator style={styles.loadingIndicator} size="small" color="#000000" /> : <Text style={styles.emptyListMessage}>Nessun risultato</Text>) : null)}
                    />
                </View>
            );
        } else {
            return (
                <View style = {{flex: 1}} onLayout = {this.setMaxHeight.bind(this)}>
                    <View style={styles.detailInteraction}>
                        <View style={styles.topView}>
                            <Text style={[styles.RCPStyle, {paddingBottom: 40}]}>Farmaco o principio attivo controllato</Text>
                            <Text style={styles.recipeTitle}>{this.props.item.company.name}</Text>
                            <Text style={styles.recipeSubTitle}>{this.props.item.data.family_name}</Text>
                        </View>
                        <View style={styles.centerView}>
                            <Text style={styles.descriptionText}>{this.state.description}</Text>
                        </View>
                        <View style={styles.bottomView}>
                            <Text style={[styles.RCPStyle, {flex: 0.3, marginTop: 10}]}>Controlla sempre gli RCP dei prodotti</Text>
                            <View style={styles.RCPView}>
                                <View style={styles.recipeView}>
                                    <Image
                                        resizeMode={'contain'}
                                        style={styles.recipeImage}
                                        source={require('../../assets/images/product_star_icon.png')}
                                    />
                                    <View style={styles.titleRecipeView}>
                                        <Text style={styles.recipeTitle}>{this.props.item.company.name}</Text>
                                        <Text style={styles.recipeSubTitle}>{this.props.item.data.family_name}</Text>
                                    </View>
                                </View>
                                <View style={styles.separatorView}></View>
                                <View style={styles.recipeView}>
                                    <Image
                                        resizeMode={'contain'}
                                        style={styles.recipeImage}
                                        source={require('../../assets/images/product_star_icon.png')}
                                    />
                                    <View style={styles.titleRecipeView}>
                                        <Text style={styles.recipeTitle}>{this.state.secondItem.name}</Text>
                                        <Text style={styles.recipeSubTitle}>{this.props.item.data.family_name}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity style = {styles.cancelTouchable} onPress = {() => this.returnToSearch()}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.cancelImage}
                            source={require('../../assets/images/freccia-sotto.png')}
                        />
                    </TouchableOpacity>
                </View>
            );
        }

    }

    returnToSearch(){
        this.setState({
            secondItem: undefined,
            description: undefined
        })
    }

    renderItem = ({ item }) => {
        return (
            <TouchableHighlight onPress={() => this.selectItem(item)}>
                <View style={styles.productRow}>
                    <Text id={item.id} url={item.url} style={styles.productTitle}>{item.name}</Text>
                </View>
            </TouchableHighlight>

        )
    }


    selectItem(item) {
        this.setState({
            secondItem: item
        }, () => {
            if (this.props.indexValue == 0) {
                this.interactionBetweenProducts(this.props.item.id, item.id)
            } else {
                this.interactionWithPrinciple(this.props.item.id, item.id)
            }
        })
    }

    interactionBetweenProducts(firstProduct, secondProduct) {
        fetch('http://dotto.bincode.it/api/products/interactions-between-products/'+firstProduct+'/'+secondProduct, {
            method: 'GET',
            headers: new Headers({
                "Authorization": `Bearer ${global.userToken}`
            })
        })
            .then((response) => response.json()).then((responseJson) => {
                if (responseJson.items.lenght > 0) {
                    this.setState({
                        description: responseJson.items[0].description
                    })
                } else {
                    this.setState({
                        description: "Non sono state trovate interazioni"
                    })
                }

            })
    }

    interactionWithPrinciple(product_id, principle_id) {
        fetch('http://dotto.bincode.it/api/products/interactions-product-active-principle/'+product_id+'/'+principle_id, {
            method: 'GET',
            headers: new Headers({
                "Authorization": `Bearer ${global.userToken}`
            })
        })
            .then((response) => response.json()).then((responseJson) => {
                if (responseJson.items.lenght > 0) {
                    this.setState({
                        description: responseJson.items[0].description
                    })
                } else {
                    this.setState({
                        description: "Non sono state trovate interazioni"
                    })
                }
                

            })
    }
}



export default class InteractionCard extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            index: 0,
            routes: [
                { key: 'first', title: 'FARMACO' },
                { key: 'second', title: 'PRINCIPIO ATTIVO' },
            ],
            currentHeight: 300
        };
    }

    renderLabel = ({ route }) => (
        <View>
            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 11, color: '#3a3a39' }}>{route.title}</Text>
        </View>
    )

    _handleIndexChange = index => this.setState({ index });

    _renderHeader = (props) =>
        <TabBar
            {...props}
            tabStyle={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}
            renderLabel={this.renderLabel}
            style={{ backgroundColor: '#F9F9F9', height: 30, justifyContent: 'center' }}
            indicatorStyle={{
                borderBottomWidth: 3,
                borderBottomColor: '#25A588',
                width: '30%',
                marginLeft: '6%',
                marginRight: '6%'
            }}

        />;

    _renderScene = SceneMap({
        first: () => <FirstRoute indexValue={0} item={this.props.item} setHeight = {(height) => this.setHeight(height)}/>,
        second: () => <FirstRoute indexValue={1} item={this.props.item} setHeight = {(height) => this.setHeight(height)}/>,
    });

    setHeight(height) {
        this.setState({
            currentHeight: height
        }, () => this.props.setHeight(height))
    }

    render() {
        return (
            <View style={{width: '100%', height: this.state.currentHeight}} >
                <TabViewAnimated
                    style={styles.container}
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    renderHeader={(this._renderHeader)}
                    onIndexChange={this._handleIndexChange}
                    initialLayout={initialLayout}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    textInput: {
        height: 40,
        marginTop: 15,
        marginBottom: 15,
        marginRight: 25,
        marginLeft: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#25A588'
    },
    productsList: {
        width: '100%',
        height: 330,
        backgroundColor: 'transparent' 
    },
    productRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 50,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#ede9e9'
    },
    productTitle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
        color: '#727474',
        paddingLeft: '10%',

    },
    favoritesIcon: {
        height: 30,
        width: '20%',
        resizeMode: 'contain'
    },
    detailInteraction: {
        flex: 1,
        justifyContent: 'center'
    },
    topView: {
        flex: 0.33,
        backgroundColor: '#F9F9F9',
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerView: {
        flex: 0.34,
        backgroundColor: '#ffffff',
        justifyContent: 'center'
    },
    bottomView: {
        flex: 0.33,
        backgroundColor: '#F9F9F9',
        justifyContent: 'space-between'
    },
    RCPStyle: {
        alignSelf: 'center',
        color: '#4A4A4A',
        textAlign: 'center'
    },
    RCPView: {
        flexDirection: 'row',
        flex: 0.7
    },
    separatorView: {
        height: '100%',
        width: 1,
        backgroundColor: '#ECECEC',
        padding: 0
    },
    recipeView: {
        flexDirection: 'row',
        justifyContent: 'center',
        flex: 1
    },
    titleRecipeView: {
        justifyContent: 'center',
        flex: 0.7
    },
    repiteTitle: {
        color: '#4A4A4A',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        
    },
    recipeSubTitle: {
        color: '#9B9B9B',
        fontFamily: 'Montserrat-Medium',
        fontSize: 8
    },
    recipeImage: {
        height: 25,
        flex: 0.3,
        alignSelf: 'center',
        margin: 5
    },
    descriptionText: {
        fontFamily: 'Montserrat-Regular',
        marginLeft: '4%',
        color: '#25A588',
        marginRight: '4%',
        alignSelf: 'center',
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 20
    },
    cancelTouchable: {
        position: 'absolute',
        right: 4,
        top: 4,
        height: 40,
        width: 40,
    },
    cancelImage: {
        height: 25,
        width: 25
    },
    emptyListMessage: {
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: '7%'
    },
    loadingIndicator: {
        marginTop: '7%'
    },
});