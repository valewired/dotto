import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import Header from '../components/Header';
import ButtonsNotifiche from '../components/ButtonsNotifiche';
import CardList from '../components/CardList';
import FooterBox from '../components/FooterBox';

export default class Notifiche extends React.Component {
    render() {
        return (
            <View style={{height:'100%'}}>
                <HeaderSearch />
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                    <ButtonsNotifiche />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                <CardList />
                </TouchableOpacity>
                <FooterBox />
            </View >
        );
    }

};