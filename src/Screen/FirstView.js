import React from 'react';
import { View, Image, StatusBar,Text } from 'react-native';
import DefaultPreference from 'react-native-default-preference';

export default class FirstView extends React.Component {

    constructor(props) {
        super(props)
        this.controlIntro=this.controlIntro.bind(this)
    }

    componentWillMount(props) {
        DefaultPreference.get('access_token').then((pippo) => {
            global.userToken = pippo
            if (pippo !== null && pippo != undefined) {
                this.controlIntro()
                this.props.navigation.navigate('Home2')
            } else {
                this.props.navigation.navigate('Registration')
            }
        })

    }

    controlIntro() {
        DefaultPreference.get('intro').then((visto) =>{
            global.showIntro = visto
            if ( visto !== null && visto!=undefined){
             console.log('ciao')
            }else{
                
             this.props.navigation.navigate('Intro')
            } 
         })
    }

    render() {
        return (
            <View style={styles.firstViewContainer}>
                <StatusBar
                    backgroundColor="#1A7B65"
                    barStyle="light-content"
                />
                <Image style={styles.logoStyle} source={require('../../assets/images/logo_app.png')} />
                <View style={styles.viewTextStyle}>
                    <Text style={styles.textStyle}>notifiche ed aggiornamenti per i </Text>
                    <Text style={styles.textStyle}>professionisti della salute</Text>
                </View>
            </View>
        )
    }
}

const styles = {
    logoStyle: {
        resizeMode: 'contain',
        width: '65%',
        height: '30%',
        marginTop:'40%'
    },
    viewTextStyle:{
        marginTop:'45%',
        alignItems:'center',
        backgroundColor:'#25A588',
        width:'62%'
    },
    firstViewContainer:{ 
        backgroundColor: '#25A588', 
        alignItems: 'center', 
        height: '100%' 
    },

    textStyle:{
        color:'#D8D8D8',
        fontFamily:'Montserrat-Regular'

    }
    
}