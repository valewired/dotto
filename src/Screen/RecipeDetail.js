import React from 'react';
import { View, TouchableOpacity, ScrollView, Text, Image,Alert ,ImageBackground, StatusBar, Modal, SectionList, Animated, TextInput, Platform, Dimensions } from 'react-native';
import Header from '../components/Header';
import CardBox from '../components/CardBox';
import Panel from '../components/Panel';
import Panel2 from '../components/Panel2';
import Search from '../components/Search';
import { StackNavigator, TabNavigator, NavigationActions } from 'react-navigation';
import InteractionCard from '../components/InteractionCard'
import LinearGradient from 'react-native-linear-gradient';
import EventRegister from '../components/EventRegister';

const width = Dimensions.get('window').width

export default class Detail extends React.Component {


    constructor(props) {
        super(props);
        const url = this.props.navigation.state.params.url

        this.state = {
            index: 0,
            routes: [
                { key: 'first', title: 'STRUMENTI' },
                { key: 'second', title: 'NOTIFICHE' },
            ],
            modalVisible: false,
            modalVisibleProfile: false,
            latitude:'',
            longitude:'',
            data: [],
            products: '',
            principle: [],
            category: '',
            item: '',
            id: '',
            product_type: '',
            expanded: false,
            currentAccordion: 0,
            precAccordion: 0,
            animation: new Animated.Value(0),
            text: '',
            minHeight: 0,
            avv: 'Avvertenze',
            effects: 'Effetti collaterali',
            pregnancy:'Gravidanza e allattamento',
            excipients:'Eccipienti',
            active_principle:'Principi attivi',
            refundability_class:'',
            atc:'',
            recipe_type:'',
            container_type:''

        };

        this.contentText = '';

        this.didPressSearch = this.didPressSearch.bind(this);
        this.didPressClose = this.didPressClose.bind(this);
        this.didPressProfile = this.didPressProfile.bind(this);
        this.didPressProfileClose = this.didPressProfileClose.bind(this)
        this.toggle = this.toggle.bind(this)
        this._setMaxHeight = this._setMaxHeight.bind(this)
        

    }

    componentWillMount() {
        let url = this.props.navigation.state.params.url
        var queryString = ''; //'?lat=' + '41.99999999' + '&lng=' + '41.9999999'; //TODO: Replace with GPS coordinates

        fetch(url + queryString, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${global.userToken}`,
            }
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({ item: responseJson })
                this.controlResponseJson(responseJson)
              
                this.productsPrinciple()
                this.sameClass()
                this.samePharmaceutical()

            });
    }

    controlResponseJson(responseJson){
        if (responseJson.product_type === null) {
            this.setState({ product_type: 'dato non disponibile' })
        } else {
            this.setState({product_type: responseJson.product_type.data.name })
        }
        if (responseJson.refundability_class === null) {
            this.setState({ refundability_class: 'dato non disponibile' })
        } else {
            this.setState({refundability_class: responseJson.refundability_class.data.partner_item_id })
        }
        if (responseJson.atc_gmp_classification === null) {
            this.setState({ atc_gmp_classification: 'dato non disponibile' })
        } else {
            this.setState({atc_gmp_classification: responseJson.atc_gmp_classification.name})
        }
        if (responseJson.recipe_type === null) {
            this.setState({ recipe_type: 'dato non disponibile' })
        } else {
            this.setState({recipe_type: responseJson.recipe_type.data.name})
        }
        if (responseJson.container_type=== null) {
            this.setState({ container_type: 'dato non disponibile' })
        } else {
            this.setState({container_type: responseJson.container_type.data.name})
        }
    }

    sameClass() {
        fetch('http://dotto.bincode.it/api/products/with-same-category/' + this.state.item.id, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${global.userToken}`,
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    category: responseJson.items
                })

            }).catch((error) => {
                console.log('sameCategory: ' + error)
            });
    }

    samePharmaceutical() {
        fetch('http://dotto.bincode.it/api/products/with-same-pharmaceutical-class/' + this.state.item.id, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${global.userToken}`,
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    data: responseJson.items
                })
             
            }).catch((error) => {
                console.log('samePharmaceutical: ' + error)
            });
    }


    productsPrinciple() {
        fetch('http://dotto.bincode.it/api/products/with-same-active-principle/' + this.state.item.id, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${global.userToken}`,
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({ principle: responseJson.items })
               

            });
    }

    didPressSearch() {
        this.setState({ modalVisible: true });
    }

    didPressClose() {
        this.setState({ modalVisible: false });
    }

    didPressProfile() {
        this.setState({ modalVisibleProfile: true });

    }

    didPressProfileClose() {
        this.setState({ modalVisibleProfile: false });

    }

    renderItem = (({ item, index }) => {

        return (

            <View style={styles.renderItemViewStyle}>

                <TouchableOpacity style={{ marginLeft: 24, marginRight: 24, flexDirection: 'row', justifyContent: 'space-between' }} onPress={() => {
                    var recipe = EventRegister.getInstance()
                    recipe.trackEvent(item.name, 'view', item.id, this.state.latitude, this.state.longitude)

                    this.props.navigation.navigate('RecipeDetail', params = { url: item.url })
                }}>
                    <View>
                        <Text id={item.id} url={item.url} style={{ fontFamily: 'Montserrat-Medium', fontSize: 12, color: '#585858' }}>{item.name}</Text>
                        <Text id={item.id} url={item.url} style={{ fontFamily: 'Montserrat-Medium', fontSize: 8, color: '#AFAFAF' }}>{item.name}</Text>
                    </View>
                    <Image
                        style={styles.buttonImage}
                        source={require('../../assets/images/freccia-sotto.png')}
                    ></Image>
                </TouchableOpacity>
            </View>
        )
    })


    setcurrentAccordion(number) {
        this.setContentText(number)
        this.setState({ currentAccordion: number })
    }


    toggle() {
        if (this.state.currentAccordion == 0) {
            return
        }
        let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

        if (this.state.expanded) {
            
            this.setState({
                expanded: !this.state.expanded,
                currentAccordion: 0
            });
        } else {
           
            this.setState({
                expanded: !this.state.expanded
            });
        }

        
        this.state.animation.setValue(initialValue);
        Animated.timing(
            this.state.animation,
            {
                toValue: finalValue,
                duration: 300
            }
        ).start();

        this.setState({
            precAccordion: this.state.currentAccordion,
        })
    }

    _setMaxHeight(event) {
        if (this.state.currentAccordion == 3) {
            this.setState({
                maxHeight: 300
            }, () => {
                if (!this.state.expanded) {
                    this.toggle()
                }
            });
        } else {
            this.setState({
                maxHeight: event.nativeEvent.layout.height
            }, () => {
                if (!this.state.expanded) {
                    this.toggle()
                }
            });
        }
        

    }

    setHeight(height) {
        this.setState({
            maxHeight: height
        }, () => {
            if (!this.state.expanded) {
                this.toggle()
            }
        });

    }

    _setMinHeight(event) {
    }

    changeView(){
        //alert(this.state.maxHeight)
        let initialValue =  this.state.maxHeight + this.state.minHeight,
            finalValue = this.state.maxHeight + this.state.minHeight;
           // alert('initial:' + initialValue)
            this.state.animation.setValue(finalValue);
            Animated.timing(
                this.state.animation,
                {
                    toValue: finalValue,
                    duration: 300
                }
            ).start();
            alert('final:' + finalValue)
    }

    


    setContentText(number) {
        if (number === 1) {
            this.setState({ text: this.state.item.data.dosage })
            this.contentText = this.state.item.data.dosage;
            this.changeView()
        } else if (number === 2) {
            this.setState({ text: this.state.item.data.contraindications })
            this.contentText = this.state.item.data.dosage;
            this.changeView()
        }
        if (number == this.state.precAccordion && !this.state.expanded) {
            alert('in if finale')
            this.toggle()
        }
    }

    didPressButton(buttonType, last_revision) {
        this.props.navigation.navigate('PackageInsert', params = last_revision);
    }

    didPressProfileButton(buttonType) {
        
        this.setState({ modalVisibleProfile: false });
        let resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home2' })]
        })

        this.props.navigation.dispatch(resetAction)
        this.props.navigation.navigate('FilePage', String(buttonType));
    }



    render() {
        let item = this.state.item
        let principle = this.state.principle
        let product_type = this.state.product_type
        if (item !== "") {
            return (
                <ScrollView contentContainerStyle={{ flexGrow: 1, flexDirection: 'column', }}>
                    <StatusBar
                        backgroundColor="#1A7B65"
                        barStyle="light-content"
                    />
                    <Header navigation={this.props.navigation} searchCallback={this.didPressSearch} profileCallback={this.didPressProfile} />
                    <Modal
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setState({
                                modalVisible: false
                            })
                        }}>
                        <Search navigation={this.props.navigation} closeCallback={this.didPressClose} />
                    </Modal>
                    <View style={styles.cardStyle}>
                        <View style={styles.viewTop}>
                            <View style={styles.topBarSubview}>
                                <Image
                                    resizeMode={'contain'}
                                    style={styles.topBarImageLeft}
                                    source={require('../../assets/images/recipe_icon.png')}
                                />
                                <Text style={styles.textTop}>{item.name}</Text>
                            </View>
                            <Image
                                resizeMode={'contain'}
                                style={styles.topBarImageRight}
                                source={require('../../assets/images/down_arrow_white.png')}
                            />
                        </View>
                        <View style={styles.secondView}>
                            <View style={styles.textView}>
                                {(this.state.currentAccordion == 3 ?
                                    <Text style={styles.title2}>
                                        {item.data.family_name}  {'\n'}
                                        <Text style={styles.title}>
                                            {item.company.name}
                                        </Text>
                                    </Text> :
                                    <Text style={styles.secondText}>
                                        RISORSE  {'\n'}
                                        <Text>
                                            DISPONIBILI
                                    </Text>
                                    </Text>
                                )}
                            </View>
                            <View style={styles.viewImage}>
                                <View style={styles.viewAcronymStyle}>
                                    <ImageBackground
                                        style={styles.imageTop}
                                        source={require('../../assets/images/icon-internet.png')}
                                        resizeMode={'contain'}
                                    >
                                        <View style={styles.badgeView}>
                                            <Text style={styles.badgeText}>!</Text>
                                        </View>
                                    </ImageBackground>
                                    <Text style={styles.acronymStyle}>WWW</Text>
                                </View>
                                <View style={styles.viewAcronymStyle}>
                                    <ImageBackground
                                        style={styles.imageTop}
                                        source={require('../../assets/images/icon-documenti.png')}
                                        resizeMode={'contain'}
                                    >
                                    </ImageBackground>
                                    <Text style={styles.acronymStyle}>DOC</Text>
                                </View>
                                <View style={styles.viewAcronymStyle}>
                                    <ImageBackground
                                        style={styles.imageTop}
                                        source={require('../../assets/images/icon-calcolatore.png')}
                                        resizeMode={'contain'}>
                                        <View style={styles.badgeView}>
                                            <Text style={styles.badgeText}>!</Text>
                                        </View>
                                    </ImageBackground>
                                    <Text style={styles.acronymStyle}>CALC</Text>

                                </View>

                                <TouchableOpacity style={styles.viewAcronymStyle} onPress={() => {
                                    this.didPressButton(4, item.last_revision)
                                }}>
                                    <ImageBackground style={styles.imageTop} source={require('../../assets/images/icon-rcp.png')}>
                                        <View style={styles.badgeView}>
                                            <Text style={styles.badgeText}>2</Text>
                                        </View>
                                    </ImageBackground>
                                    <Text style={styles.acronymStyle}>RCP</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                        <View style={styles.thirdView}>
                            <Panel title={item.company.name} >
                                <Text style={styles.centerBoldText}>{item.data.family_name}{'\n'}</Text>
                                <Text style={styles.centerText} >{item.data.description}{'\n'}</Text>
                                <Text style={styles.bottomText}>{item.data.therapeutic_indications} {'\n'}</Text>
                                <Text style={styles.titleText}>Tipo di prodotto:{'\n'}</Text>
                                <Text style={styles.content}>{product_type} {'\n'}</Text>
                                <Text style={styles.titleText}>Principio attivo: {'\n'}</Text>
                                <Text style={styles.content}>{item.data.active_principles} {'\n'}</Text>
                                <Text style={styles.titleText}>Prezzo: {'\n'}</Text>
                                <Text style={styles.content}>{item.data.national_price_to_public_1} {'\n'}</Text>
                                <Text style={styles.titleText}>Classe: {'\n'}</Text>
                                <Text style={styles.content}>{this.state.refundability_class} {'\n'}</Text>
                                <Text style={styles.titleText}>ATC:{'\n'}</Text>
                                <Text style={styles.content}>{this.state.atc_gmp_classification} {'\n'}</Text>
                                <Text style={styles.titleText}>Ricetta:{'\n'}</Text>
                                <Text style={styles.content}>{this.state.recipe_type} {'\n'}</Text>
                                <Text style={styles.titleText}>SSN:{'\n'}</Text>
                                <Text style={styles.content}>{product_type} {'\n'}</Text>
                                <Text style={styles.titleText}>Forma: {'\n'}</Text>
                                <Text style={styles.content}>{item.pharmaceutical_form_type.data.name} {'\n'}</Text>
                                <Text style={styles.titleText}>Contenitore:{'\n'}</Text>
                                <Text style={styles.content}>{this.state.container_type} {'\n'}</Text>
                                <Text style={styles.titleText}>Conservazione:{'\n'}</Text>
                                <Text style={styles.content}>{item.data.storage} {'\n'}</Text>
                                <Text style={styles.titleText}>Scadenza:{'\n'}</Text>
                                <Text style={styles.content}>{this.state.product_type} {'\n'}</Text>
                                <Text style={styles.titleText}>Nota: {'\n'}</Text>
                                <Text style={styles.content}>{this.state.product_type} {'\n'}</Text>
                            <View style={styles.containerFlag}>
                                 {item.data.gluten_flag === false &&  <Image style={{ height: 40, width: 40, marginLeft: '5%' }} source={require('../../assets/images/gluten_icon_off.png')} />
                                
                                } 
                                 {item.data.gluten_flag === true &&  <Image style={{ height: 40, width: 40, marginLeft: '5%' }} source={require('../../assets/images/gluten_icon_on.png')} />
                              
                               }
                                <Text style={styles.textGluten}>Glucosio</Text> 
                                    {item.data.lactose_flag=== false &&  <Image style={{ height: 40, width: 40, marginLeft: '5%' }} source={require('../../assets/images/lactose_icon_off.png')} />
                                   
                               
                                }
                                  {item.data.lactose_flag === true && <Image style={{ height: 40, width: 40, marginLeft: '5%' }} source={require('../../assets/images/lactose_icon_on.png')} />
                                  
                                }
                                    <Text style={styles.textGluten}>Lattosio</Text>
                                    {item.data.gluten_flag === false &&  <Image style={{ height: 40, width: 40, marginLeft: '5%' }} source={require('../../assets/images/glucose_icon_off.png')} />
                                   
                              
                                }
                                  {item.data.gluten_flag === true &&  <Image style={{ height: 40, width: 40, marginLeft: '5%' }} source={require('../../assets/images/glucose_icon_on.png')} />
                                
                                } 
                            <Text style={styles.textGluten}>Glutine</Text>
                                </View>
                               
                               
                                <TouchableOpacity style={styles.touchDetail} onPress={() =>{
                                    Alert.alert("Avvertenze",item.data.warnings_and_precautions)
                                }}>
                                <Text style={styles.textDetail}>Avvertenze e precauzioni</Text>
                                </TouchableOpacity>
                               

                                <TouchableOpacity  style={styles.touchDetail} onPress={() =>{
                                    Alert.alert("Effetti collaterali",item.data.side_effects)
                                }}>
                                <Text style={styles.textDetail}>Effetti collaterali</Text>
                                </TouchableOpacity>

                                   

                                 <TouchableOpacity style={styles.touchDetail} onPress={() =>{
                                    Alert.alert("Gravidanza e allattamento",item.data.pregnancy_and_breastfeeding)
                                }}>
                                <Text style={styles.textDetail}>Gravidanza e allattamento</Text>
                                </TouchableOpacity>

                                  

                                 <TouchableOpacity style={styles.touchDetail} onPress={() =>{
                                    Alert.alert("Eccipienti",item.data.excipients)
                                }}>
                                <Text style={styles.textDetail}>Eccipienti</Text>
                                </TouchableOpacity>


                                 <TouchableOpacity style={styles.touchDetail} onPress={() =>{
                                    Alert.alert("Principi attivi",item.data.wactive_principles)
                                }}>
                                <Text  style={styles.textDetail}>Principi attivi</Text>
                                </TouchableOpacity>


                              
                              
                                <View style={styles.viewRcpStyle}>
                                    <Image style={{ height: 40, width: 40}} source={require('../../assets/images/rcp_icon.png')}/>
                                    <View style={{marginLeft:'5%', justifyContent:'center'}}>
                                    <Text style={styles.rcpTextStyle}>Controlla l'RCP aggiornato per maggiori dettagli</Text>
                                    <Text style={styles.content}>vuoto</Text>
                            </View> 
                                </View>
                            
                            </Panel>
                        </View>
                        <LinearGradient style={styles.bottomView} colors={['#F9F9F9', '#ECECEC']}>
                            <TouchableOpacity style={[styles.touchableOpacityBox, (this.state.currentAccordion == 1) && styles.enabledCard]} onPress={this.setcurrentAccordion.bind(this,1)}>
                                <Image
                                    style={styles.imageBottom}
                                    source={require('../../assets/images/posologia_icon.png')}
                                    resizeMode={'contain'} />
                                <Text numberOfLines={1} style={[styles.textBottom, { fontSize: width / 45 }]}>POSOLOGIA</Text>
                                {(this.state.currentAccordion == 1) &&
                                    <View style={{ height: 3, backgroundColor: '#25A588', position: 'absolute', bottom: 0, left: 0, right: 0 }} />
                                }
                            </TouchableOpacity>
                            <View style={styles.separatorView} />
                            <TouchableOpacity style={[styles.touchableOpacityBox, (this.state.currentAccordion == 2) && styles.enabledCard]} onPress={this.setcurrentAccordion.bind(this,2)}>
                                <Image
                                    style={styles.imageCentralBottom}
                                    source={require('../../assets/images/controindicazioni_icon.png')}
                                    resizeMode={'contain'} />
                                <Text numberOfLines={1} style={[styles.textBottom, { fontSize: width / 45 }]}>CONTROINDICAZIONI</Text>
                                {(this.state.currentAccordion == 2) &&
                                    <View style={{ height: 3, backgroundColor: '#25A588', position: 'absolute', bottom: 0, left: 0, right: 0 }} />
                                }
                            </TouchableOpacity>
                            <View style={styles.separatorView} />
                            <TouchableOpacity style={[styles.touchableOpacityBox, (this.state.currentAccordion == 3) && styles.enabledCard]} onPress={this.setcurrentAccordion.bind(this,3)}>
                                <Image
                                    style={styles.imageBottom}
                                    source={require('../../assets/images/interazioni_icon.png')}
                                    resizeMode={'contain'} />
                                <Text numberOfLines={1} style={[styles.textBottom, { fontSize: width / 45 }]}>INTERAZIONI</Text>
                                {(this.state.currentAccordion == 3) &&
                                    <View style={{ height: 3, backgroundColor: '#25A588', position: 'absolute', bottom: 0, left: 0, right: 0 }} />
                                }
                                {/* <View onLayout={this._setMaxHeight.bind(this)} style={{ width: '100%' }}>
                                    <TextInput
                                        style={{ width: '80%', color: '#ffffff', fontFamily: 'Montserrat-Regular' }}
                                        underlineColorAndroid={'#ffffff'}
                                        placeholder={'Inserisci prodotto'}
                                        placeholderTextColor={'#75c2ae'}
                                        onChangeText={(text) => {
                                            //let testo = this.state.text
                                            let data = this.state.data
                                            console.log(this.state.text)
                                            console.log(this.state.item.id)
                                            if (this.state.text !== "") {
                                                fetch('http://dotto.bincode.it/api/products/search?q=' + this.state.text, {
                                                    method: 'GET'
                                                })
                                                    .then((response) => response.json())
                                                    .then((responseJson) => {


                                                        this.setState({ products: responseJson.items })
                                                        console.log(responseJson)


                                                        console.log(products.items)
                                                    })
                                                this.setState({ text })
                                                console.log(this.state.text)
                                            } else {
                                                console.log('data è vuoto')
                                                this.setState({ text })
                                                console.log(this.state.text)
                                            }
                                        }
                                        }
                                        value={this.state.text}
                                    />
                                </View>*/}
                            </TouchableOpacity>
                        </LinearGradient>
                        <Animated.View
                            style={[{ height: this.state.animation }]}>
                            <View style={{ overflow: 'hidden' }}>
                                {this.state.currentAccordion == 3 ?
                                    <View onLayout={this._setMaxHeight.bind(this)} >
                                        <InteractionCard item={this.state.item} setHeight={(height) => this.setHeight(height)} />
                                    </View>
                                    :
                                    <Text style={{ padding: 20 }} onLayout={this._setMaxHeight.bind(this)}>{this.state.text}</Text>
                                }
                            </View>

                        </Animated.View>
                    </View>
                    <View>
                        <Panel2 items={this.state.principle} title="Farmaci con lo stesso " title2="PRINCIPIO ATTIVO">
                            <SectionList
                                style={styles.sectionListStyle}
                                keyExtractor={(item, index) => item.id}
                                renderItem={this.renderItem}
                                sections={[
                                    { data: this.state.principle }
                                ]}
                            />
                        </Panel2>
                        <Panel2 items={this.state.category} title="Farmaci con la stessa " title2="CLASSE FARMACEUTICA">
                            <SectionList
                                style={styles.sectionListStyle}
                                keyExtractor={(item, index) => item.id}
                                renderItem={this.renderItem}
                                sections={[
                                    { data: this.state.category }
                                ]}
                            />
                        </Panel2>
                        <Panel2 items={this.state.data} title="Farmaci con la stessa " title2="CATEGORIA">
                            <SectionList
                                style={styles.sectionListStyle}
                                keyExtractor={(item, index) => item.id}
                                renderItem={this.renderItem}
                                sections={[
                                    { data: this.state.data }
                                ]}
                            />
                        </Panel2>
                    </View>
                </ScrollView>
            );
        } else {
            return (
                <View></View>
            )
        }
    }

}


const styles = {
    touchPress: {
        width: '100%',
        height: 320,
    },
    touchableWhiteOpacityStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#f8f8f8',
        width: '100%',
        height: 48,
        borderBottomWidth: 1,
        borderBottomColor: '#bdb8b8',
    },
    viewText: {
        marginLeft: '5%',
        justifyContent: 'center',
        width: '80%',
        height: 40
    },

    viewTextLittleStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 7,
        color: '#d4cece'
    },

    viewTextStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
        color: '#7c7c7c',
        marginTop: '3%'
    },

    iconStyle: {
        resizeMode: 'contain',
        marginTop: '2%',
        width: '10%',
        height: 20,
        marginRight: '5%',
    },
    cardStyle: {
        backgroundColor: '#fafafa',
    },



    cardShadowStyle: {
        shadowColor: "#000000",
        shadowOpacity: 3,
        shadowRadius: 6,
        shadowOffset: {
            height: -2,
            width: -4
        }
    },

    viewTop: {
        backgroundColor: '#00876b',
        width: '100%',
        height: 25,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center'

    },

    secondView: {
        flexDirection: 'row',
        width: '100%',
        height: 60,
        alignItems: 'center',
        backgroundColor: '#ffffff'
    },

    textTop: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 10,
        marginLeft: '7%',
        color: 'white'
    },

    secondText: {
        fontFamily: 'Montserrat-Regular',
        marginLeft: '4%',
        color: '#25A588',
        marginRight: '4%'
    },

    imageTop: {

        width: 22,
        height: 22,
        alignSelf: 'center'
    },

    viewImage: {
        flexDirection: 'row',
        width: '55%',
        justifyContent: 'space-between',
        marginLeft: '5%',
    },

    viewAcronymStyle: {
        alignItems: 'center',
        marginTop: '7%',
        justifyContent: 'center',
        marginRight: 5,
        height: 55,
        width: 40
    },

    acronymStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 8,
        alignSelf: 'center',
        marginTop: 4
    },

    thirdView: {
        marginTop: '2%',
        backgroundColor:'#fafafa'
    },

    centerText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
        color: '#ACACAC',
        marginTop: 8
    },

    centerBoldText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 15,
        marginTop: 8,
        color: '#565656'

    },

    bottomText: {
        fontFamily: 'Montserrat-Regular',
        marginRight: '2%',
        fontSize: 13,
        color: '#959595',
        marginTop: 8
    },

    bottomView: {
        marginTop:20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopWidth: 1,
        borderTopColor: '#d4cece',
    },

    imageBottom: {
        height: 30,
        width: 30,
        alignSelf: 'center',
        marginBottom: 8,

        //marginLeft:'3%'
    },

    imageCentralBottom: {
        height: 30,
        width: 40,
        marginBottom: 8,

    },

    textBottom: {
        marginTop: '1%',
        fontSize: 10,
        fontFamily: 'Montserrat-Bold',
        color: '#25A588',
        width: '100%',
        height: 15,
        textAlign: 'center'
    },

    touchableOpacityBox: {
        flex: 0.33,
        alignItems: 'center',
        alignSelf: 'stretch',
        padding: '2%'
    },


    textView: {
        width: '40%'
    },
    continerCircle: {
        height: 70,
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0)',
        overflow: 'hidden',
    },
    circle: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        backgroundColor: '#70d2d1',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        top: 8
    },
    touchPress: {
        width: '100%',
        height: 320,
    },

    contentContainer: {

    },
    touchableWhiteOpacityStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#f8f8f8',
        width: '100%',
        height: 48,
        borderBottomWidth: 1,
        borderBottomColor: '#bdb8b8',
    },
    viewText: {
        marginLeft: '5%',
        justifyContent: 'center',
        width: '80%',
        height: 40
    },

    viewTextLittleStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 7,
        color: '#d4cece'
    },

    viewTextStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
        color: '#7c7c7c',
        marginTop: '3%'
    },

    iconStyle: {
        resizeMode: 'contain',
        marginTop: '2%',
        width: '10%',
        height: 20,
        marginRight: '5%',
    },

    badgeView: {
        backgroundColor: '#E94E1B',
        height: 16,
        width: 16,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: '60%',
        top: -8
    },

    badgeText: {
        fontSize: 10,
        color: '#ffffff',
        fontWeight: '900',
    },

    separatorView: {
        height: '100%',
        width: 1,
        backgroundColor: '#ECECEC',
        padding: 0
    },
    buttonImage: {
        resizeMode: 'contain',
        width: 20,
        height: 20,
    },
    sectionListStyle: {
        paddingTop: 15,
        paddingBottom: 15
    },

    enabledCard: {
        backgroundColor: '#ffffff'
    },

    title: {
        marginLeft: '4%',
        marginRight: '4%',
        color: '#585858',
        fontFamily: 'Montserrat-Medium',
        fontSize: 14
    },
    title2: {
        marginLeft: '4%',
        marginRight: '4%',
        color: '#ACACAC',
        fontFamily: 'Montserrat-Medium',
        fontSize: 10
    },
    renderItemViewStyle: {
        height: 40,
        width: '100%',
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#e5e5e5'
    },

    renderItemTouchable: {
        marginLeft: 24,
        marginRight: 24,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    topBarImageLeft: {
        height: 18,
        width: 18,
        marginLeft: 5
    },

    topBarImageRight: {
        height: 7,
        width: 7,
        marginRight: 10
    },

    topBarSubview: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        height: '100%',
        alignItems: 'center'
    },

    titleText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 13,
        marginTop: 10,
        color:'#606060'
       
    },

    content: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 11,
        color: '#bdbdbd',
       
    },

    containerFlag: {
        height: 80,
        width: '100%',
        flexDirection: 'row',
        justifyContent:'space-around',
        alignItems:'center'
    },

    viewRcpStyle:{
        flexDirection:'row',
        backgroundColor:'#ffffff',
        alignItems:'center',
        marginLeft:'2%',
        height:80,
        width:'100%'
    
    },
    rcpTextStyle:{
        color: '#25A588',
        fontSize:11,
        fontFamily: 'Montserrat-Bold'
    },

    touchDetail:{
        backgroundColor:'#f4f4f4',
        width:'100%',
        height: 30,
        justifyContent:'center',
        borderBottomWidth:1,
        borderBottomColor:'#fdfdfd'
    },
   textDetail:{
       marginLeft:'5%',
       fontFamily:'Montserrat-Regular',
       color:'#7a7a7a'
   },

   textGluten:{
       color:'#747474'
   }

}
