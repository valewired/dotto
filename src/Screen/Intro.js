import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

import Swiper from 'react-native-swiper';
import DefaultPreference from 'react-native-default-preference';

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    height:'100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
 
  text: {
    color: '#969696',
    fontSize: 15,
    fontFamily: 'Montserrat-Regular',
    width:'65%',
    textAlign: 'center',
  },

  button:{
      width:'100%',
      height: 40,
      backgroundColor:'#00a886',
      alignItems:'center',
      justifyContent:'center',
      borderBottomWidth: 1,
      borderBottomColor:'black'
  },
  textButton:{
      color:'#ffffff'
  }
})

export default class Slide extends Component {
  constructor(props){
     super(props)
     this.state={id:0}
      this.skipIntro=this.skipIntro.bind(this)
  }

  skipIntro(){
    DefaultPreference.set('intro', true.toString());
    this.props.navigation.navigate('Home2');
    global.showIntro = true
   
  }


  render(){
    return (
        <View style={{height:'100%'}}>
      <Swiper style={styles.wrapper} showsButtons={true} loop={false}>
        <View style={styles.slide1} >
          <Image style={{height:'50%',width:'65%'}} source={require('../../assets/images/intro_1.png')}/>
         
          <Text style={styles.text}>ricevi inviti ed eventi a convegni di tuo interesse</Text>
      
        </View>
        <View style={styles.slide1}>
        <Image style={{height:'50%',width:'65%'}} source={require('../../assets/images/intro_2.png')}/>
          <Text style={styles.text}>accedi ai siti e portali di prodotti dedicati ad HCP senza dover ricordare le credenziali</Text>
        </View>
        <View style={styles.slide1} id={2}>
        <Image style={{height:'50%',width:'65%'}} source={require('../../assets/images/intro_3.png')}/>
          <Text style={styles.text}>ricevi informazioni dei medicinali che utilizzi direttamente sul tuo cellulare</Text>
        </View>
        <View style={styles.slide1} id={3}>
        <Image style={{height:'50%',width:'65%'}} source={require('../../assets/images/intro_4.png')}/>
          <Text style={styles.text}>comunica con gli informatori scientifici in modo rapido e veloce</Text>
        </View>
        <View style={styles.slide1} id={4}>
        <Image style={{height:'50%',width:'65%'}} source={require('../../assets/images/intro_5.png')}/>
          <Text style={styles.text}>ricevi notifiche in tempo reale sui prodotti che utilizzi per non perdere mai gli aggiornamenti</Text>
        </View>
        <View style={styles.slide1} id={5} scrollEnabled={ false }>
        <Image style={{height:'50%',width:'65%'}} source={require('../../assets/images/intro_6.png')}/>
          <Text style={styles.text}>accedi a calcolatori medici e di dosagio per delle prescrizioni sempre accurate</Text>
        </View>
      </Swiper>
      
   <TouchableOpacity  style={styles.button} onPress={() =>{
     this.skipIntro()
   }}>
       <Text style={styles.textButton}>Ho capito!</Text>
   </TouchableOpacity>
   </View>
    );
  }
}