import React from 'react';
import {View, Text, Image, TouchableHighlight } from 'react-native';

export default class Conferma extends React.Component{
    render(){
        return (
            <View style={{backgroundColor:'#25A588', alignItems:'center',height:'100%'}}>
                <Image style={{marginTop:'25%', resizeMode:'contain', height:'20%',width:'30%'}} source={require('../../assets/images/smile.png')}/>
                <Text style={{color:'#ffffff', fontFamily:'Montserrat-Bold', fontSize:15,marginTop:'5%'}}>Account creato</Text>
                <TouchableHighlight style={styles.button} onPress={ () => {
                                this.props.navigation.navigate('Home2')
                            }}>
                            <Text style={{ color: '#ffffff', fontFamily: 'Montserrat-Bold' }}>Continua</Text>
                        </TouchableHighlight>
            </View>
        )
    }
}


const styles={
    button: {
        width: '80%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e94e1b',
        padding: 10,
        marginTop: '10%',
        borderRadius: 25
    },
}
