import * as React from 'react';
import { View, StyleSheet, Dimensions, Text, Modal } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import Header from '../components/Header';
import Search from '../components/Search';
import SearchPage from '../components/SearchPage';

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

const FirstRoute = () => {
return (
<View style={[ styles.container, { backgroundColor: '#ffffff' } ]} >
   <SearchPage />
                     </View>
)
}
const SecondRoute = () => <View style={[ styles.container, { backgroundColor: '#673ab7' } ]} />;
const ThirdRoute= () => <View style={[styles.container, {backgroundColor: 'orange'}]} />;

export default class TabViewExample extends React.Component {

  
  constructor(props) {
    super(props);
  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'Popolari' },
      { key: 'second', title: 'Ricarcati' },
      { key: 'third', title: 'Per Area' }
    ],
    modalVisible: false,
    data: ''
  };
  this.didPressSearch = this.didPressSearch.bind(this);
  this.didPressClose = this.didPressClose.bind(this)
}


  didPressSearch() {
    this.setState({ modalVisible: true });
}

didPressClose() {
    this.setState({ modalVisible: false });
}


  renderLabel = ({ route }) => (
    <View>
      <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 12, color: '#ffffff'}}>{route.title}</Text> 
    </View>
  )

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = (props) => <TabBar 
  {...props}
  tabStyle={{flexDirection: 'row', justifyContent:'center', alignItems: 'center'}}
  renderLabel={this.renderLabel}
  style={{ backgroundColor: '#25A588', height: '8%'}}
  indicatorStyle={{
    marginTop: '5%',
    borderBottomWidth: 3,
    borderBottomColor: '#ffffff',
    backgroundColor:'#25A588'
  }}
  />;

  _renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute
  });

  render() {
    return (
      <View style={{ height: '100%' }}>
        <Header navigation={this.props.navigation} searchCallback={this.didPressSearch} />
                    <Modal
                        //animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setState({
                                modalVisible: false
                            })
                        }}>
                        <Search navigation={this.props.navigation} closeCallback={this.didPressClose} />
                    </Modal>
        <TabViewAnimated
          style={styles.container}
          navigationState={this.state}
          renderScene={this._renderScene}
          renderHeader={this._renderHeader}
          onIndexChange={this._handleIndexChange}
          initialLayout={initialLayout}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
