import React from 'react';
import { View, Image, TextInput, Text, TouchableHighlight } from 'react-native';
import DefaultPreference from 'react-native-default-preference';

export default class Login extends React.Component{

    state = { email: 'nuovo@gmail.com', password:'123123',id:'' }

    onPressButton(event) {
        let email= this.state.email
        let password= this.state.password
        fetch( 'http://dotto.bincode.it/api/users/login?' + JSON.stringify(email) + JSON.stringify(password), {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
            
                this.setState({
                    id: responseJson[0].access_token
                })
             
                DefaultPreference.set('access_token',responseJson[0].access_token);
                this.props.navigation.navigate('Home2')
                global.userToken= responseJson[0].access_token
            }).catch((error) => {
                alert(
                    'Errore!!'
                )
              })
        

    }

   


    render(){
        return (
            <View style={styles.pageStyle}>
                 <View style={styles.containerImage}>
                    <Image style={styles.bannerStyle} source={require('../../assets/images/logo_app.png')} />
                </View>
                <Text style={styles.labelStyle}>Email</Text>
                <View style={styles.viewTextInputStyle}>
                    <TextInput
                        underlineColorAndroid={'#25a588'}
                        style={styles.textInputStyle}
                        placeholder={'Email'}
                        placeholderTextColor={'#dbf0eb'}
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                    />
                </View>
                <Text style={styles.labelStyle}>Password</Text>
                        <View style={styles.viewTextInputStyle}>
                            <TextInput
                                style={styles.textInputStyle}
                                secureTextEntry={true}
                                underlineColorAndroid={'#25a588'}
                                placeholderTextColor={'#dbf0eb'}
                                placeholder={'Password'}
                                value={this.state.password}
                                onChangeText={password => this.setState({ password })}
                            />
                        </View>
                <TouchableHighlight style={styles.button} onPress={this.onPressButton.bind(this)}>
                    <Text style={{color:'#ffffff', fontFamily:'Montserrat-Bold'}}>Accedi</Text>
                </TouchableHighlight>
                </View>
        )
    }
}


const styles={
    pageStyle: {
        backgroundColor: '#25a588',
        height: '100%',
        alignItems: 'center'
    },

    containerImage: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '3%',
        marginBottom: '5%',
        height: '15%',
        width: '50%'
    },

    bannerStyle: {
        resizeMode: 'contain',
        height: '70%',

    },

    labelStyle: {
        color: '#b8e1d8',
        marginTop: '3%'
    },


    viewTextInputStyle: {
        width: '100%',
        height: '7%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#b8e1d8',
        borderBottomWidth: 1,
    },

    textInputStyle: {
        //marginTop: -10,
        marginBottom: '1%',
        width: '100%',
        fontFamily: 'Montserrat-Bold',
        color: '#ffffff',
        textAlign:'center',

    },

    button: {
        width: '80%',
        height:'8%',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: '#e94e1b',
        padding: 10,
        marginTop:'15%',
        borderRadius: 25
    },
}
