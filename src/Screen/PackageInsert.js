import React from 'react';
import { View, Text, WebView, ScrollView, Image,TouchableOpacity } from 'react-native';
import Panel2 from '../components/Panel2';

export default class PackageInsert extends React.Component {

  constructor(props) {
    super(props);
    console.log(this.props.navigation.state.params)

    this.state = {
      position: 0,
      id: '',
      interval: null,
      testo:'',
      openAccordion: -1,
      arrayImages: this.props.navigation.state.params.data.slider_images,
      denomination: this.props.navigation.state.params.data.denomination,
      pharmaceutical_form: this.props.navigation.state.params.data.pharmaceutical_form,
      name: this.props.navigation.state.params.data.name,
      filling_date: this.props.navigation.state.params.data.filling_date,
      description: this.props.navigation.state.params.data.description,
      composition: this.props.navigation.state.params.data.composition,
      clinic_informations: this.props.navigation.state.params.data.clinic_informations,
      pharmacological_properties: this.props.navigation.state.params.data.pharmacological_properties,
      pharmacological_informations: this.props.navigation.state.params.data.pharmacological_informations,
      authorization_holder: this.props.navigation.state.params.data.authorization_holder,
      authorization_number: this.props.navigation.state.params.data.authorization_number,
      authorization_date: this.props.navigation.state.params.data.authorization_date,
      revision_date: this.props.navigation.state.params.data.revision_date,
      dataSource: [
        {
          title: 'Title 1',
          caption: 'Caption 1',
          url: this.props.navigation.state.params.data.slider_images[0].normalized.file,
        }, {
          title: 'Title 2',
          caption: 'Caption 2',
          url: 'https://upload.wikimedia.org/wikipedia/commons/6/6a/Lago_bolsena_tramonto.jpg',
        }, {
          title: 'Title 3',
          caption: 'Caption 3',
          url: 'http://placeimg.com/640/480/any',
        }, {
          title: 'Title 2',
          caption: 'Caption 2',
          url: 'https://upload.wikimedia.org/wikipedia/commons/6/6a/Lago_bolsena_tramonto.jpg',
        }, {
          title: 'Title 3',
          caption: 'Caption 3',
          url: 'http://placeimg.com/640/480/any',
        }
      ],
    };
  }


  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
        });
      }, 2500)
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

 
  
  open(text,id,web){
    let openAccordion=this.state.openAccordion
    let testo=this.state.testo
    
    if (openAccordion === -1){
    this.setState({openAccordion: id})
    this.setState({testo: text})
   
    }
    else if (openAccordion !== id){
      this.setState({openAccordion: id})
      this.setState({testo: text})
    }
}




  render() {
    let position = this.state.position
    return (
      <ScrollView>
        <View style={styles.viewNameStyle}>
          <Text style={styles.filling_dateStyle}>{this.state.filling_date}</Text>
          <Text style={styles.nameStyle}>{this.state.name}</Text>
          <Text style={styles.descriptionStyle}>{this.state.description}</Text>
          <View>
            <Image source={{}} />
          </View>
        </View>
        <View style={styles.viewPanel}>
        <Panel2 title="1. " title2="COMPOSIZIONE QUALITATIVA E QUANTITATIVA" items={this.state.denomination} id={1} openPanel={this.open.bind(this)}>
         { (this.state.openAccordion == 1) && 
         <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
          </Panel2>
          <Panel2 title="2. " title2="COMPOSIZIONE QUALITATIVA E QUANTITATIVA" items={this.state.composition} id={2}  openPanel={this.open.bind(this)}>
          { (this.state.openAccordion == 2) && 
         <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
          
          </Panel2>
          <Panel2 items={this.state.data} title="3. " title2="FORMA FARMAUCETICA" items={this.state.description} id={3}  openPanel={this.open.bind(this)}>
          { (this.state.openAccordion == 3) && 
         <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
          </Panel2>
          <Panel2 title="4. " title2="INFORMAZIONI CLINICHE" items={this.state.clinic_informations} id={4} web={this.state.url} openPanel={this.open.bind(this)}>
          { (this.state.openAccordion == 4) && 
         <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
          </Panel2>
          <Panel2 title="5. " title2="PROPRIETA FARMACOLOGICHE" items={this.state.pharmacological_properties}  id={5}  openPanel={this.open.bind(this)}>
          { (this.state.openAccordion == 5) && 
         <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
          </Panel2>
          <Panel2 title="6. " title2="INFORMAZIONI FARMACEUTICHE"  items={this.state.pharmacological_informations}  id={6}  openPanel={this.open.bind(this)}>
          { (this.state.openAccordion == 6) && 
         <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
          </Panel2>
          <Panel2 title="7. " title2="INFORMAZIONI FARMACEUTICHE" items={this.state.authorization_holder}  id={7}  openPanel={this.open.bind(this)}>
          { (this.state.openAccordion == 7) && 
          <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
          </Panel2>
          <Panel2 title="8. " title2="INFORMAZIONI FARMACEUTICHE"   items={this.state.authorization_number} id={8}  openPanel={this.open.bind(this)}>
          { (this.state.openAccordion == 8) && 
          <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
            
          </Panel2>
          <Panel2 title="9. " title2="INFORMAZIONI FARMACEUTICHE"   items={this.state.authorization_date} id={9}  openPanel={this.open.bind(this)}>
          { (this.state.openAccordion == 9) && 
          <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
          </Panel2>
          <Panel2 title="10. " title2="INFORMAZIONI FARMACEUTICHE"  items={this.state.revision_date} id={10}  openPanel={this.open.bind(this)}>
          { (this.state.openAccordion == 10) && 
          <View>
         <Text>{this.state.testo}</Text>
         <WebView
              //source={{ uri: }}
              style={{ height: 100 }}
            />
         </View>}
          </Panel2>
        </View>

      </ScrollView>
    );
  }
}


const styles = {
  viewNameStyle: {
    backgroundColor: '#ffffff',
    marginTop: '2%',
    marginLeft: '2%',
    marginRight: '2%'
  },
  filling_dateStyle: {
    marginLeft: '3%',
    marginTop: '1%',
    color: '#b6b7b5',
    fontFamily: 'Montserrat-Regular',
    fontSize: 11
  },
  nameStyle: {
    marginLeft: '3%',
    marginTop: '1%',
    fontFamily: 'Montserrat-Regular',
    fontSize: 13,
    color: '#606260'
  },
  descriptionStyle: {
    color: '#7c7d7c',
    marginLeft: '3%',
    marginTop: '1%',
    marginBottom: '2%',
    fontFamily: 'Montserrat-Regular',
    fontSize: 11,
  },

  viewPanel: {
    marginLeft: '2%',
    marginRight: '2%'
  }

}
