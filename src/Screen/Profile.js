import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Dimensions } from 'react-native';

export default class Profile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            specializzazione:'',
            json: '',
            profileDelegate: props.profileCallback,
        }
        console.log(this.props)
        //this.ProfileDelegate = props.pressCallback;
        //this.closeProfileDelegate = props.closeProfileCallback;
        this.navigationDelegate=this.navigationDelegate.bind(this)

        this.navigationSystem = this.props.navigation;
    }

    componentWillMount() {
        fetch('http://dotto.bincode.it/api/users/profile', {
            method: 'GET',
            headers: {
                "Authorization": 'Bearer ' + global.userToken,
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({ json: responseJson })
                this.setState({ specializzazione: responseJson.data.company_name })

            }).catch((error) => {
                console.error(error)
            })
    }


    navigationDelegate(buttonType) {
        console.log('Pressed footer button ' + buttonType);
        this.setState({ modalVisibleProfile: false });
        // this.props.navigation.navigate('FilePage', String(buttonType));


        let resetAction = NavigationActions.reset({
            index: 1,
            actions: [
                NavigationActions.navigate({ routeName: 'Home2'}),
                NavigationActions.navigate({ routeName: 'FilePage'})
                        ]
        })
        this.navigationSystem.dispatch(resetAction)
      }




    render() {
       
        return (
            <View style={styles.principleViewStyle}>
                <View style={styles.centralStyle}>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate('Home2')
                }} style={styles.touchCloseIcon}>
                    <Image style={styles.closeIconStyle} source={require('../../assets/images/close_icon.png')} />
                </TouchableOpacity>
                    <TouchableOpacity style={styles.touchImageStyle} onPress={() => this.props.navigation.navigate('InfoUser', params = { image: this.state.data.images })}>
                        <Image style={styles.imageStyle} source={{ uri: this.state.json.account_images }} />
                        <View style={{flexDirection:'row'}}>
                        <Text style={styles.nameStyle}>{this.state.json.name} {'\n'}</Text>
                        <Text style={styles.nameStyle}>{this.state.json.surname}</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.specializationViewStyle}>
                    <Text style={styles.specializationStyle}>Specializzazione associate: {this.state.specializzazione}</Text>
                    <Text style={styles.researchStyle}>RICERCA FARMACO // CALCOLATORE</Text>
                    </View>
                    <View style={styles.farmStyle}>
                    <View style={styles.viewButtonSearchStyle}>
                        <Text style={styles.buttonTitle}>FARMACI :</Text>
                        <TouchableOpacity style={styles.TouchableStyle}>
                            <Text style={styles.textTouchStyle}> PREFERITI </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.TouchableStyle}>
                            <Text style={styles.textTouchStyle}> ULTIMI VISTI </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.viewButtonSearchStyleDue}>
                        <Text style={styles.buttonTitle}>CALCOLATORI :</Text>
                        <TouchableOpacity style={styles.TouchableStyle}>
                            <Text style={styles.textTouchStyle}> PREFERITI </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.TouchableStyle}>
                            <Text style={styles.textTouchStyle}> ULTIMI VISTI </Text>
                        </TouchableOpacity>
                    </View>
                    </View>
                </View>
                <View style={styles.bottomStyle}>
                <View style={styles.viewResourcesStyle}>
                    <Text style={styles.textResources}>LE MIE RISORSE</Text>
                </View>
                <View style={styles.viewBottomStyle}>
                    <TouchableOpacity style={styles.calculatorStyle}>
                        <Text style={styles.calculatorTextStyle}>CALCOLATORI</Text>
                        <View style={styles.badgeView}>
                            <Text style={styles.badgeText}>3</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.eventStyle} onPress={() => { this.navigationDelegate(2) }}>
                        <Text style={styles.eventTextStyle}>EVENTI</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.eventStyle} onPress={() => { this.navigationDelegate(1) }}>
                        <Text style={styles.eventTextStyle}>SITI WEB</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.eventStyle} onPress={() => { this.navigationDelegate(0)}}>
                        <Text style={styles.eventTextStyle}>DOCUMENTI</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.eventStyle}>
                        <Text style={styles.eventTextStyle}>COMUNICAZIONI</Text>
                    </TouchableOpacity>
                </View>
                </View>
            </View>
        )
    }
}

const styles = {
    principleViewStyle: {
        backgroundColor: '#25A588',
        alignItems: 'center',
        height: '100%',
        width: '100%'
    },

    centralStyle: {
        alignItems: 'center',
        height: '50%',
        width: '100%'
    },

    touchImageStyle: {
        backgroundColor: '#25A588',
        alignItems: 'center', 
        height: '45%', 
        width: '100%', 

    },

    imageStyle: {
        alignItems: 'center',

        height: '65%',
        width: '24%',
        borderRadius: Dimensions.get('window').height * 0.07,
        borderWidth: 1,
        marginBottom: '2%'
    },

    nameStyle: {
        //fontFamily: 'Montserrat-Regular',
        color: '#ffffff',
        fontSize: 16,
        marginBottom: '2%'
    },

    specializationStyle: {
        //fontFamily: 'Montserrat-Regular',
        color: '#ffffff',
        fontSize: 11,
        marginBottom: '4%'
    },

    closeIconStyle: {
        marginLeft: '87%',
        marginTop:'2%',
        height: '55%',
        width: '8%'
    },

    researchStyle: {
        //fontFamily: 'Montserrat-Regular',
        color: '#ffffff',
        fontSize: 15,
    },

    viewButtonSearchStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        height: '9%',
        marginTop:'3%'
    },
    viewButtonSearchStyleDue: {
        flexDirection: 'row',
        alignItems: 'center',
        height: '9%',
        marginTop:'6%'
    },
    buttonTitle: {
        //fontFamily: 'Montserrat-Regular',
        color: '#ffffff',
        fontSize: 15
    },
    TouchableStyle: {
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#ffffff',
        marginLeft: '2%',
        marginRight: '2%',
        padding: '1%'
    },

    textTouchStyle: {
        color: '#f3e500',
        fontSize: 14
    },

    viewResourcesStyle: {
        backgroundColor: '#1A7B65',
        width: '100%',
        height:'10%',
        alignItems: 'center',
    },

    textResources: {
        color: '#ffffff',
        fontSize: 15,
        padding: '1%'
    },
    viewBottomStyle: {
        backgroundColor: '#ffffff',
        height: '100%',
        width: '100%',
        alignItems: 'center',
    },
    calculatorStyle: {
        marginTop: '6%',
        flexDirection: 'row',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#d5d5d5',
        width: '80%',

    },

    calculatorTextStyle: {
        // fontFamily: 'Montserrat-Regular',
        fontSize: 15,

    },

    badgeView: {
        backgroundColor: '#E94E1B',
        height: 18,
        marginTop: '1%',
        width: 18,
        borderRadius: 9,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: '3%',
        marginBottom: '3%',
        marginTop: 0
    },

    badgeText: {
        fontSize: 15,
        color: '#ffffff',
        fontWeight: '200',
    },

    eventStyle: {
        marginTop: '3%',
        flexDirection: 'row',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#d5d5d5',
        width: '80%',
    },

    eventTextStyle: {
        // fontFamily: 'Montserrat-Regular',
        fontSize: 15,
        marginBottom: '3%'
    },

    bottomStyle:{
        width:'100%',
        height:'50%',
        marginTop: '8%',
        backgroundColor: '#1A7B65'
    },

    touchCloseIcon:{
        height: '12%', 
        width: '100%',
        marginTop:'3%'
    
    },
    specializationViewStyle:{
        height:'18%',
        width:'100%',
        alignItems: 'center', 
    },

    topView:{
        height:'12%',
        width:'100%',
        backgroundColor: '#25A588',
        alignItems: 'center', 
    },
    farmStyle:{
        height:'40%',
        width:'100%',
        backgroundColor: '#25A588',
        alignItems: 'center', 
    }
}