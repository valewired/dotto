import React from 'react';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import { View, TouchableOpacity, Text, Image, Modal, SectionList, Platform, Linking, Animated, Dimensions } from 'react-native';
import Header from '../components/Header';
import Search from '../components/Search';
import EventRegister from '../components/EventRegister';
import DefaultPreference from 'react-native-default-preference';
import Swipeout from 'react-native-swipeout';
import Picker from 'react-native-picker';

export default class FilePage extends React.Component {

    constructor(props) {
        super(props);
        this.documentTypes = ["documents", "websites", "company-events", "calculators"];
        var index = parseInt(props.navigation.state.params);
        var doctype = this.documentTypes[index];

        this.state = {
            modalVisible: false,
            modalVisibleProfile: false,
            documents: [],
            currentType: doctype,
            selectedCompany: -1,
            filter: 'TUTTI',
            companies: [],
            _rowX: {},
            latitude:null,
            longitude:null
        };
    
        this.didPressSearch = this.didPressSearch.bind(this);
        this.didPressClose = this.didPressClose.bind(this);
        this.didPressProfile = this.didPressProfile.bind(this);
        this.didPressProfileClose = this.didPressProfileClose.bind(this)

        this.initPicker = this.initPicker.bind(this);
        this.showCompanyPicker = this.showCompanyPicker.bind(this);
        this.company2Code = this.company2Code.bind(this);
        this.companyCode2Name = this.companyCode2Name.bind(this);

        this.getLocalPath = this.getLocalPath.bind(this);
        this.openFile = this.openFile.bind(this);
    }

    componentWillMount() {
        this.reloadDocuments(-1);
    }

    reloadDocuments(filter) {
        let filterString = (this.state.selectedCompany == -1) ? '' : '?filters[companies]=' + filter;
        fetch('http://dotto.bincode.it/api/' + this.state.currentType + filterString, {
            method: 'GET',
            headers: new Headers({
                "Authorization": `Bearer ${global.userToken}`,
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                documents: responseJson.items
            })
            this.setState({
                companies: responseJson.companies
            })
            this.initPicker(responseJson.companies)
        }).catch((error) => {
            alert('Errore. Impossibile accedere al contenuto.')
        })

        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => {
                this.setState({ error: error.message })
                alert(error)
            });
        
    }

    didPressSearch() {
        this.setState({ modalVisible: true });
    }

    didPressClose() {
        this.setState({ modalVisible: false });
    }

    didPressProfile() {
        this.setState({ modalVisibleProfile: true });
    }
    
    didPressProfileClose() {
        this.setState({ modalVisibleProfile: false });
    }

    initPicker(companyList) {
        let companyNames = companyList.map(
          c => { return c.name }
        );
        companyNames.splice(0, 0,  "Nessuna");
  
        Picker.init({
          pickerTitleText: `Filtra per azienda`,
                pickerData: companyNames,
                selectedValue: [this.state.selectedCompany],
                onPickerConfirm: data => {
                    let companyID = this.company2Code(data[0]);
                    this.setState({ 
                        selectedCompany: companyID 
                    })
                    this.reloadDocuments(companyID);
                },
                onPickerCancel: data => {
                },
                onPickerSelect: data => {
                    console.log("onPickerSelect " + data);
                }
        });
    }
  
    showCompanyPicker() {
        Picker.show();
    }

    company2Code(companyName) {
        if (companyName == undefined || this.state.companies == undefined || this.state.companies.length == 0) {
          return "";
        }
        var res = this.state.companies.filter(function(c) {
          return c.name == companyName;
        });
        return res[0].id;
      }

    companyCode2Name(companyId) {
        if (this.state.companies == undefined || this.state.companies.length == 0) {
          return "Caricamento dati in corso";
        }
        var res = this.state.companies.filter(function(c) {
          return c.id == companyId;
        });
        return (res.length > 0) ? res[0].name : `Filtra per azienda`;
    }

    getFile(currentType) {
        this.state.currentType = currentType
        fetch('http://dotto.bincode.it/api/' + currentType, {
            method: 'GET',
            headers: new Headers({
                "Authorization": `Bearer ${global.userToken}`,
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({ documents: responseJson.items })
        }).catch((error) => {
            alert('Errore. Impossibile accedere al contenuto.')
        })
    }

    getLocalPath (url) {
        const filename = url.split('/').pop();
        // feel free to change main path according to your requirements
        return `${RNFS.DocumentDirectoryPath}/${filename}`;
    }

    openFile(link, name, created, id) {

         // http://www.h2app.it/misc/demo1.pdf
        const url = link + ".pdf"; //'http://www.h2app.it/misc/test2.pdf';
        const localFile = this.getLocalPath(url);
           
        const options = {
            fromUrl: url,
            toFile: localFile
        };
        RNFS.downloadFile(options).promise
          .then(() => {
              console.log("Download completed");
              FileViewer.open(localFile)
              .then(() => {
                // success
                console.log("File opened!!!");
            })
            .catch(error => {
                // error
                console.log("Error opening file :(");
            })
          });




        /*
        DefaultPreference.get(name).then((created) => {
            if (created !== null) {
                const localFile = getLocalPath(name);
                function getLocalPath(file_name) {
                    return `${RNFS.DocumentDirectoryPath}/${file_name}`;
                }
                FileViewer.open(localFile)
            } else {
                let userDefault = this.state.userDefault
                var queryString = '?lat=' + '41.99999999' + '&lng=' + '41.9999999'; //TODO: Replace with GPS coordinates
                const url = link + queryString;

                const full_path = 'http://www.h2app.it/misc/demo1.pdf'; // `${RNFS.DocumentDirectoryPath}/${name}`;
                console.log("Full path: " + full_path);
                // const localFile = getLocalPath(name);


                FileViewer.open(full_path)
            }
        }).catch(error => {
            DefaultPreference.set(name, created);
            let userDefault = this.state.userDefault
            function getLocalPath(file_name) {
                return `${RNFS.DocumentDirectoryPath}/${file_name}`;
            }
            const url = link;
            const localFile = getLocalPath(name);
            FileViewer.open(localFile)
        });
        */
    }

    openEvent(image, title, description, created, actions,id) {
        var docu=EventRegister.getInstance()
        docu.trackEvent(title,'view', id)
        this.props.navigation.navigate("EventDetail", params = { image, title, description, created, actions })
    }

    openWebsites(url, title, created,id) {
        var docu=EventRegister.getInstance()
        docu.trackEvent(url,'view', id)
        Linking.canOpenURL(url).then(() => { return Linking.openURL(url); })
    }

    htmlEntities(str) {
        return String(str).replace(/&/g, '').replace(/</g, '').replace(/>/g, '').replace(/"/g, '');
    }

    deleteFile(currentType, id){
        if (currentType === 'documents'){
          
            this.state.documents.pop(id)
            
        }
    }

    renderItem = ({ item }) => {
        let str=item.description
        this.htmlEntities(str)
        str=this.htmlEntities(str)
        let currentType = this.state.currentType
        this.state._rowX[item.id] = new Animated.Value(0)
        var swipeoutBtns = [
            {
                text: 'Preferiti',
                backgroundColor: 'green',
                onPress: () => {
                    fetch('http://dotto.bincode.it/api/' + currentType + '/favorites/' + item.user_object_id + '/1', {
                        method: 'PATCH',
                        headers: new Headers({
                            "Authorization": `Bearer ${global.userToken}`,
                        })
                        ,
                        body: JSON.stringify({
                            id: item.user_object_id
                        })
                    })
                        .then((response) => response.json())
                        .then((responseJson) => {
                  
                        })
                    
                }
            },
            {
                text: 'Delete',
                backgroundColor: 'red',
                onPress: () => {
                    Animated.timing(this.state._rowX[item.id], {
                        toValue  : Dimensions.get('window').width,
                        duration : 500
                    }).start(() => {
                        var array = this.state.documents // make a separate copy of the array
                        var index = array.indexOf(item)
                        array.splice(index, 1);
                        this.setState({documents: array});
                    });
                    //TODO: - Decommentare per eliminare il documento
                    /*fetch('http://dotto.bincode.it/api/' + currentType + '/delete/' + item.user_object_id + '/1', {
                        method: 'PATCH',
                        headers: new Headers({
                            "Authorization": `Bearer ${global.userToken}`,
                        })
                        ,
                        body: JSON.stringify({
                            id: item.user_object_id
                        })
                    })
                        .then((response) => response.json())
                        .then((responseJson) => {
                            console.log(responseJson);
                            console.log(item.user_object_id)
                            this.deleteFile(currentType, item.user_object_id)
                        }).catch((error) => {
                            alert(
                                console.log(error)
                            )
                        })*/
                }
            }
        ]
        return (
            <Animated.View key = {item.id} style = {{right: this.state._rowX[item.id]}}>
            <Swipeout right={swipeoutBtns} style={{marginBottom: 5}} >
                <TouchableOpacity style={this.TouchableOpacityStyle(currentType)} onPress={() => {
                    if (currentType === "documents") {
                        this.openFile(item.download_file_url, item.download_meta.name, item.created_at, item.id)
                    } else if (currentType === "company-events") {
                        this.openEvent(item.image, item.title, item.description, item.created_at, item.actions, item.id)
                    } else if (currentType === "websites") {
                        this.openWebsites(item.url, item.title, item.created_at,item.id)
                    }
                }
                }>
                    <View style={styles.viewContainerFile}>
                        <Image source={{ uri: item.icon }} style={styles.imageIconStyle} />
                        <View style={styles.viewText}>
                        <Text style={styles. itemDescription}>{str}</Text>
                            <Text style={styles.itemTitle}>{item.title}</Text>
                        </View>
                    </View>
                
                </TouchableOpacity>
            </Swipeout>
            </Animated.View>
        )
    }

    TouchableOpacityStyle(currentType) {
      
        if (currentType === 'documents') {
            return {
                width: '95%',
                height: 60,
                marginLeft: '2.5%',
                //marginRight: '2%',
                elevation: 8,
                borderLeftWidth: 4,
                borderLeftColor: '#39b59c',
                backgroundColor: '#ffffff'
            }
        }
        else if (currentType === 'websites') {
            return {
                width: '95%',
                height: 60,
                marginLeft: '2.5%',
                marginTop: '2%',
                elevation: 8,
                borderLeftWidth: 4,
                borderLeftColor: '#fcc30d',
                backgroundColor: '#ffffff'
            }
        } else if (currentType === 'company-events') {
            return {
                width: '95%',
                height: 60,
                marginLeft: '2.5%',
                marginTop: '2%',
                elevation: 8,
                borderLeftWidth: 4,
                borderBottomColor: '#4496f2',
                backgroundColor: '#ffffff'
            }
        }
    }

    getFavorites(data) {
        fetch('http://dotto.bincode.it/api/' + this.state.currentType + '?filters' + '[' + data + ']', {
            method: 'GET',
            headers: new Headers({
                "Authorization": `Bearer ${global.userToken}`,
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({ documents: responseJson.items })
             
            }).catch((error) => {
                alert('Errore!!')
            })
    }

    styleForTab(tabType, color) {
    
        if (this.state.currentType === tabType) {
            return {
                borderBottomColor: color,

            }
        }
        else {
            return {
               borderBottomColor: '#dddddd'
            }
        }
    }

    getPickerItems=()=> {
          console.log('dentro')
          console.log(this.state.companies)
          let comps = this.state.companies
        if (comps.length > 0) {

            return `<Picker.Item  key=1 label="SHIRE" value="SHIRE"/>`;
            ;
        }
        
    }

    render() {
       
        let currentType = this.state.currentType
        let documents = this.state.documents
        return (
            <View>
                <Header navigation={this.props.navigation} searchCallback={this.didPressSearch} profileCallback={this.didPressProfile} />
                <Modal
                   
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({
                            modalVisible: false
                        })
                    }}>
                    <Search navigation={this.props.navigation} closeCallback={this.didPressClose} />
                </Modal>
                <View style={styles.companyFilter}>
                    <TouchableOpacity style = {styles.companyFilter} onPress={() => this.showCompanyPicker()}>
                        <Text
                            style = {[{textAlign: 'center', backgroundColor: 'white', color: 'black'}]}
                            editable = {false}
                            placeholder = "Seleziona un'azienda"
                            underlineColorAndroid='transparent'>{this.companyCode2Name(this.state.selectedCompany)}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewBottom}>
                    <TouchableOpacity style={styles.touchStyle} onPress={() => {
                        this.getFile("documents")
                    }}>
                        <View style={[this.styleForTab("documents", "#25A588"), {  borderBottomWidth: 3, alignItems: 'center', width: '100%' }]}>
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 12 }}>documenti</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchStyleSite} onPress={() => {
                      
                        this.getFile("websites")
                    }}>
                        <View style={[this.styleForTab("websites", "#39b59c"), {  borderBottomWidth: 3, alignItems: 'center', width: '100%' }]}>
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 12 }}>siti</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchStyleEvents} onPress={() => {

                        this.getFile("company-events")
                     
                    }}>
                        <View style={[this.styleForTab("company-events", "#fcc30d"), {  borderBottomWidth: 3, alignItems: 'center', width: '100%' }]}>
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 12 }}>eventi</Text>
                        </View>
                    </TouchableOpacity >
                    <TouchableOpacity style={styles.touchStyleCalc}>
                        <View style={[this.styleForTab("calcolatori", '#4496f2'), { borderBottomWidth: 3, alignItems: 'center', width: '100%' }]}>
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 12 }}>calcolatori</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchStyleFilter} onPress={() => {
                        if (this.state.filter === 'TUTTI') {
                            this.setState({
                                filter: 'PREFERITI'
                            })
                            this.getFavorites("favorites")
                        } else {
                            this.setState({
                                filter: 'TUTTI'
                            })
                            this.getFavorites()
                        }
                    }}>
                        <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 13 }}>{this.state.filter}</Text>
                    </TouchableOpacity>
                </View>
                <SectionList
                    style={{ height: '100%', width: '100%' }}
                    keyExtractor={(item, index) => item.id}
                    renderItem={this.renderItem}
                    sections={[
                        { data: this.state.documents }
                    ]}
                />
            </View>
        )
}
}


const styles = {
    viewBottom: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 30,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        marginBottom: '2%'
    },
    companyFilter: {
        width: '100%',
        height: 55,
        justifyContent: 'center',
        backgroundColor: 'white',
        borderColor: 'white',
        borderBottomColor: '#E1E1E1',
        borderWidth: 1.5
    },
    viewPicker: {
        width: '100%',
        height: 330,
        alignItems: 'center',
        backgroundColor: 'fuchsia'
    },
    touchStyle: {
        width: '20%',
        marginLeft: '2%',
     
    },
    touchStyleSite: {
        width: '12%',
        marginLeft: '2%',
    },
    touchStyleEvents: {
        width: '12%',
        marginLeft: '2%',

     
    },
    touchStyleCalc: {
        width: '20%',
        marginLeft: '2%',
      
    },

    touchStyleFilter: {
        width: '24%',
        height: 30,
        marginLeft: '4%',
        alignItems: 'center',
        justifyContent: 'center',
        borderLeftWidth: 2,
        borderLeftColor: '#d3d2d2',
    },
    TouchableOpacityStyle: {
        width: '95%',
        height: 70,
        marginLeft: '2.5%',
        marginTop: '2%',
        elevation: 8,
        borderLeftWidth: 4,
        borderLeftColor: '#39b59c',
        backgroundColor: '#ffffff'

    },
    viewContainerFile: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        aspectRatio: 5,
        marginLeft: '4%',
        marginBottom: '2%'
    },
    viewText: {
        flexDirection: 'column',
        marginLeft: '10%',
        width: '60%',
    },
    itemTitle: {
        textAlign: 'left',
        fontFamily: 'Montserrat-Bold'
    },
    itemDescription: {
        fontSize:10,
        textAlign: 'left',
        fontFamily: 'Montserrat-Regular'
    },
    imageIconStyle: {
        height: 20,
        width: 20,
    }
}
