import * as React from 'react';
import { View, StyleSheet, Dimensions, Text, TouchableOpacity, Image, TextInput, TouchableHighlight, ScrollView, DatePickerIOS, DatePickerAndroid, Platform, PlatformIOS } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import Header from '../components/Header';
import ImagePicker from 'react-native-image-picker';

const initialLayout = {
    height: 0,
    width: Dimensions.get('window').width,
};

class FirstRoute extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            nome: '', cognome: '', email: '', telefono: '', confermaMail: '', password: '', confermaPassword: '', message: '', os: '',
            data: {}, categories: [], filtro: [], latitude: null, longitude: null, error: null, chosenDate: new Date(), avatarSource: '', json: ''
        }


        this.setDate = this.setDate.bind(this);
        this.showCamera = this.showCamera.bind(this)
        this.requestCameraPermission = this.requestCameraPermission.bind(this)
        this.setParentState = props.callback;
        this.getParentState = props.getCallback;

    }

    async requestCameraPermission() {
        try {

            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    'title': 'Cool Photo App Camera Permission',
                    'message': 'Cool Photo App needs access to your camera ' +
                        'so you can take awesome pictures.'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the camera")
            } else {
                console.log("Camera permission denied")
            }
        } catch (err) {
            console.warn(err)
        }
    }

    componentWillMount(props) {
        if (Platform.OS === 'android') {
            this.requestCameraPermission()
        }



    }

    setDate(newDate) {
        let chosenDate = this.state.chosenDate
        alert('data:' + chosenDate)
        this.setState({ chosenDate: newDate })
    }


    showCamera() {
        var ImagePicker = require('react-native-image-picker');

        // More info on all the options is below in the README...just some common use cases shown here
        var options = {
            title: 'Select Avatar',
            customButtons: [
                { name: 'fb', title: 'Choose Photo from Facebook' },
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        /**
         * The first arg is the options object for customization (it can also be null or omitted for default options),
         * The second arg is the callback which sends object: response (more info below in README)
         */
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source.uri
                });
            }
        });
    }

    render() {
        return (
            <View style={styles.personalDate}>
                <TouchableOpacity style={styles.containerImage} onPress={() => {
                    this.showCamera()
                }}>
                    <Image source={{ uri: this.state.avatarSource }} style={styles.uploadAvatar} />
                </TouchableOpacity>
                <Text style={styles.labelStyle}>Nome</Text>
                <View style={styles.viewTextInputStyle}>
                    <TextInput
                        underlineColorAndroid={'#25a588'}
                        style={styles.textInputStyle}
                        placeholder={'Nome'}
                        placeholderTextColor={'#dbf0eb'}
                        value={this.getParentState('nome')}
                        onChangeText={name => this.setParentState({ nome: name })}
                    />
                </View>
                <Text style={styles.labelStyle}>Cognome</Text>
                <View style={styles.viewTextInputStyle}>
                    <TextInput
                        underlineColorAndroid={'#25a588'}
                        style={styles.textInputStyle}
                        placeholder={'Cognome'}
                        placeholderTextColor={'#dbf0eb'}
                        value={this.getParentState('cognome')}
                        onChangeText={surname => this.setParentState({ cognome: surname })}
                    />
                </View>
                <Text style={styles.labelStyle}>Data di nascita</Text>
                <View style={styles.viewTextInputStyle}>

                </View>

                <Text style={styles.labelStyle}>Indirizzo email</Text>
                <View style={styles.viewTextInputStyle}>
                    <TextInput
                        style={styles.textInputStyle}
                        underlineColorAndroid={'#25a588'}
                        placeholderTextColor={'#dbf0eb'}
                        placeholder={'mail'}
                        value={this.getParentState('email')}
                        onChangeText={email => this.setParentState({ email: email })}
                    />
                </View>
                <Text style={styles.labelStyle}>Telefono</Text>
                <View style={styles.viewTextInputStyle}>
                    <TextInput
                        style={styles.textInputStyle}
                        underlineColorAndroid={'#25a588'}
                        placeholderTextColor={'#dbf0eb'}
                        placeholder={'Telefono'}
                        value={this.getParentState('telefono')}
                        onChangeText={tele => this.setParentState({ telefono: tele })}
                    />
                </View>
            </View >)
    }
}
class SecondRoute extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            albo: '', specialization: '', add: false, add1: false
        }

        this.addSpecialization = this.addSpecialization.bind(this)
        this.closeAdd = this.closeAdd.bind(this)
        this.setParentState = props.callback;
        this.getParentState = props.getCallback;
    }



    addSpecialization() {
        let add = this.state.add

        if (add == false) {
            this.setParentState({ add: true })
            this.setState({ add: true })
        } else {
            this.setParentState({ add1: true })
            this.setState({ add1: true })
        }
    }

    closeAdd(select) {
        if (select === 1) {
            this.setParentState({ add: false })
            this.setState({ add: false })
        } else if (select === 2) {
            this.setParentState({ add1: false })
            this.setState({ add1: false })
        }
    }

    render() {
        return (
            <View style={styles.personalDate}>
                <Text style={styles.labelStyle}>N° Iscrizione albo</Text>
                <View style={styles.viewTextInputStyle}>
                    <TextInput
                        style={styles.textInputStyle}
                        underlineColorAndroid={'#25a588'}
                        placeholderTextColor={'#dbf0eb'}
                        placeholder={'Iscrizione albo'}
                        value={this.getParentState('albo')}
                        onChangeText={health_code => this.setParentState({ albo: health_code })}
                    />
                </View>
                <Text style={styles.labelStyle}>Anno di Laurea</Text>
                <View style={styles.viewTextInputStyle}>

                </View>

                <Text style={styles.labelStyle}>Specializzazione principale</Text>
                <View style={styles.viewSpecialization}>
                    <TextInput
                        style={styles.textSpecialization}
                        underlineColorAndroid={'#25a588'}
                        placeholderTextColor={'#dbf0eb'}
                        placeholder={'Specializzazione'}
                        value={this.getParentState('specialization')}
                        onChangeText={company_name => this.setParentState({ specialization: company_name })}
                    />
                    <TouchableOpacity style={styles.badgeView} onPress={() => {
                        this.addSpecialization()
                    }}>
                        <Text style={styles.badgeText}>+</Text>
                    </TouchableOpacity>
                </View>
                {this.state.add === true && <View style={styles.viewSpecialization}>
                    <TextInput
                        style={styles.textSpecialization}
                        underlineColorAndroid={'#25a588'}
                        placeholderTextColor={'#dbf0eb'}
                        placeholder={'Specializzazione'}
                        value={this.state.specialization}
                        onChangeText={specialization => this.setParentState({ specialization })}
                    />
                    <TouchableOpacity style={styles.badgeView} onPress={() => {
                        this.closeAdd(1)
                    }}>
                        <Text style={styles.badgeText}>x</Text>
                    </TouchableOpacity>
                </View>}
                {this.state.add1 === true && <View style={styles.viewSpecialization}>
                    <TextInput
                        style={styles.textSpecialization}
                        underlineColorAndroid={'#25a588'}
                        placeholderTextColor={'#dbf0eb'}
                        placeholder={'Specializzazione'}
                        value={this.state.specialization}
                        onChangeText={specialization => this.setParentState({ specialization })}
                    />
                    <TouchableOpacity style={styles.badgeView} onPress={() => {
                        this.closeAdd(2)
                    }}>
                        <Text style={styles.badgeText}>x</Text>
                    </TouchableOpacity>
                </View>}

            </View >
        )
    }
}
class ThirdRoute extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            hospital: '', street: '', cap: '', city: '', tel: '', emailWork: '',
            studio: '', capStudio: '', cityStudio: '', streetStudio: '', telStudio: '', addStudio: false
        }

        this.addStudio = this.addStudio.bind(this)
        this.setParentState = props.callback;
        this.getParentState = props.getCallback;

    }


    addStudio() {
        let addStudio = this.state.addStudio
        this.setParentState({ addStudio: !addStudio })
        this.setState({ addStudio: !addStudio })
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: '#25a588' }}>
                <View style={{ backgroundColor: '#25a588', alignItems: 'center', marginTop: '5%' }}>
                    <Text style={styles.labelStyle}>Nome Ospedale/Clinica</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            underlineColorAndroid={'#25a588'}
                            style={styles.textInputStyle}
                            placeholder={'Ospedale'}
                            placeholderTextColor={'#dbf0eb'}
                            value={this.getParentState('hospital')}
                            onChangeText={is_company => this.setParentState({ hospital: is_company })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Via Ospedale/Clinica</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            underlineColorAndroid={'#25a588'}
                            style={styles.textInputStyle}
                            placeholder={'Via/Viale'}
                            placeholderTextColor={'#dbf0eb'}
                            value={this.state.strada}
                            onChangeText={strada => this.setParentState({ street: strada })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Cap Ospedale/Clinica</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            underlineColorAndroid={'#25a588'}
                            style={styles.textInputStyle}
                            placeholder={'00100'}
                            placeholderTextColor={'#dbf0eb'}
                            value={this.state.capNumber}
                            onChangeText={capNumber => this.setParentState({ cap: capNumber })}
                        />
                    </View>

                    <Text style={styles.labelStyle}>Città Ospedale/Clinica</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'città'}
                            value={this.getParentState('city')}
                            onChangeText={work_location => this.setParentState({ city: work_location })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Telefono di lavoro</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'Telefono di lavoro'}
                            value={this.state.telNumber}
                            onChangeText={telNumber => this.setParentState({ tel: telNumber })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Email di lavoro</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'email.lavoro@esempio.it'}
                            value={this.getParentState('emailWork')}
                            onChangeText={email => this.setParentState({ emailWork: email })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Studio Privato</Text>
                    <View style={styles.viewSpecialization}>
                        <TextInput
                            underlineColorAndroid={'#25a588'}
                            style={styles.textSpecialization}
                            placeholder={'Nome studio'}
                            placeholderTextColor={'#dbf0eb'}
                            value={this.state.studioSede}
                            onChangeText={studioSede => this.setParentState({ studio: studioSede })}
                        />
                        <TouchableOpacity style={styles.badgeView} onPress={() => {
                            this.addStudio()
                        }}>
                            <Text style={styles.badgeText}>+</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.labelStyle}>Via</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            underlineColorAndroid={'#25a588'}
                            style={styles.textInputStyle}
                            placeholder={'Via/Viale'}
                            placeholderTextColor={'#dbf0eb'}
                            value={this.state.streetS}
                            onChangeText={streetS => this.setParentState({ streetStudio: streetS })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Cap </Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            underlineColorAndroid={'#25a588'}
                            style={styles.textInputStyle}
                            placeholder={'00100'}
                            placeholderTextColor={'#dbf0eb'}
                            value={this.state.capS}
                            onChangeText={capS => this.setState({ capStudio: capS })}
                        />
                    </View>

                    <Text style={styles.labelStyle}>Città</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'città'}
                            value={this.state.cityS}
                            onChangeText={cityS => this.setState({ cityStudio: cityS })}
                        />
                    </View>
                    <Text style={styles.labelStyle}>Telefono dello studio</Text>
                    <View style={styles.viewTextInputStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            underlineColorAndroid={'#25a588'}
                            placeholderTextColor={'#dbf0eb'}
                            placeholder={'Telefono di lavoro'}
                            value={this.state.telS}
                            onChangeText={telS => this.setParentState({ telStudio: telS })}
                        />
                    </View>
                    {this.state.addStudio === true &&
                        <View style={{ backgroundColor: '#25a588', alignItems: 'center', width: '100%' }}>
                            <Text style={styles.labelStyle}>Studio Privato</Text>
                            <View style={styles.viewSpecialization}>
                                <TextInput
                                    underlineColorAndroid={'#25a588'}
                                    style={styles.textSpecialization}
                                    placeholder={'Nome studio'}
                                    placeholderTextColor={'#dbf0eb'}
                                    value={this.state.studioSede}
                                    onChangeText={studioSede => this.setParentState({ studio: studioSede })}
                                />
                                <TouchableOpacity style={styles.badgeView} onPress={() => {
                                    this.addStudio()
                                }}>
                                    <Text style={styles.badgeText}>x</Text>
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.labelStyle}>Via</Text>
                            <View style={styles.viewTextInputStyle}>
                                <TextInput
                                    underlineColorAndroid={'#25a588'}
                                    style={styles.textInputStyle}
                                    placeholder={'Via/Viale'}
                                    placeholderTextColor={'#dbf0eb'}
                                    value={this.state.streetS}
                                    onChangeText={streetS => this.setParentState({ streetStudio: streetS })}
                                />
                            </View>
                            <Text style={styles.labelStyle}>Cap Studio</Text>
                            <View style={styles.viewTextInputStyle}>
                                <TextInput
                                    underlineColorAndroid={'#25a588'}
                                    style={styles.textInputStyle}
                                    placeholder={'00100'}
                                    placeholderTextColor={'#dbf0eb'}
                                    value={this.state.capS}
                                    onChangeText={capS => this.setState({ capStudio: capS })}
                                />
                            </View>

                            <Text style={styles.labelStyle}>Città</Text>
                            <View style={styles.viewTextInputStyle}>
                                <TextInput
                                    style={styles.textInputStyle}
                                    underlineColorAndroid={'#25a588'}
                                    placeholderTextColor={'#dbf0eb'}
                                    placeholder={'città'}
                                    value={this.state.cityS}
                                    onChangeText={cityS => this.setState({ cityStudio: cityS })}
                                />
                            </View>
                            <Text style={styles.labelStyle}>Telefono dello studio</Text>
                            <View style={styles.viewTextInputStyle}>
                                <TextInput
                                    style={styles.textInputStyle}
                                    underlineColorAndroid={'#25a588'}
                                    placeholderTextColor={'#dbf0eb'}
                                    placeholder={'Telefono di lavoro'}
                                    value={this.state.telS}
                                    onChangeText={telS => this.setParentState({ telStudio: telS })}
                                />
                            </View>
                        </View>}
                </View>
            </ScrollView >
        )
    }
}

export default class TabViewExample extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            routes: [
                { key: 'first', title: 'DATI ANAGRAFICI' },
                { key: 'second', title: 'PROFESSIONE' },
                { key: 'third', title: 'SEDE DI LAVORO' }
            ],
            /*First tab*/
            nome: '',
            cognome: '',
            email: '',
            telefono: '',
            confermaMail: '',
            password: '',
            confermaPassword: '',
            message: '',
            os: '',
            data: '',
            categories: [],
            filtro: [],
            latitude: null,
            error: null,
            chosenDate: new Date(),
            avatarSource: '',
            /*second tab*/
            albo: '',
            specialization: '',
            add: false,
            add1: false,
            /*third tab*/
            hospital: '',
            street: '',
            cap: '',
            city: '',
            tel: '',
            emailWork: '',
            studio: '',
            capStudio: '',
            cityStudio: '',
            streetStudio: '',
            telStudio: '',
            addStudio: false,
            json: ''
        }

        this.setParentState = this.setParentState.bind(this);
        this.getParentState = this.getParentState.bind(this);
    }

    renderLabel = ({ route }) => (
        <View>
            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 11, color: '#3a3a39' }}>{route.title}</Text>
        </View>
    )

    componentWillMount() {
        fetch('http://dotto.bincode.it/api/users/profile', {
            method: 'GET',
            headers: {
                "Authorization": 'Bearer ' + global.userToken,
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {

                this.setState({ nome: responseJson.name })
                this.setState({ cognome: responseJson.surname })
                this.setState({ email: responseJson.email })
                this.setState({ albo: responseJson.health_code })
                this.setState({ specialization: responseJson.data.company_name })
                this.setState({ hospital: responseJson.is_company })
                this.setState({ city: responseJson.data.work_location })
                this.setState({ emailWork: responseJson.working_places[0].email })
                /*per il momento*/
                this.setState({ data: responseJson.birthday })



            }).catch((error) => {
                alert('errore:' + error)
            })

    }


    setParentState(keyAndValue) {
        this.setState(keyAndValue);
        console.log(keyAndValue)

    }

    getParentState(key) {
        return this.state[key];
    }

    save() {
        alert(this.state.cognome)
        let nome = this.state.nome
        let cognome = this.state.cognome

        fetch('http://dotto.bincode.it/api/users/profile' + JSON.stringify(nome) + JSON.stringify(cognome), {
            method: 'PUT',
            headers: new Headers({
                "Authorization": `Bearer ${global.userToken}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }),
            body: JSON.stringify({
                nome: nome,
                cognome: cognome
            })
        }).then((response) => response.json()).then((responseJson) => {
            alert('modifica effettuata')

        }).catch((error) => {
            alert('Errore:' + error)

        })


    }



    _handleIndexChange = index => this.setState({ index });

    _renderHeader = (props) =>
        <TabBar
            {...props}
            tabStyle={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}
            renderLabel={this.renderLabel}
            //renderBadge={this.renderBadge}
            style={{ backgroundColor: '#ffffff', height: '8%', justifyContent: 'center' }}
            indicatorStyle={{
                borderBottomWidth: 3,
                borderBottomColor: '#25A588'
            }}

        />;

    _renderScene = ({ route }) => {
        switch (route.key) {
            case 'first':

                return <FirstRoute callback={this.setParentState} getCallback={this.getParentState} />;
            case 'second':
                return <SecondRoute callback={this.setParentState} getCallback={this.getParentState} />;
            case 'third':
                return <ThirdRoute callback={this.setParentState} getCallback={this.getParentState} />;
            default:
                return null;
        }
    };




    render() {
        return (
            <View style={{ height: '100%' }}>
                <View style={{ height: '90%' }}>
                    <Header navigation={this.props.navigation} searchCallback={this.didPressSearch} profileCallback={this.didPressProfile} />
                    <TabViewAnimated style={{ height: '90%' }}
                        navigationState={this.state}
                        renderScene={this._renderScene}
                        renderHeader={this._renderHeader}
                        onIndexChange={this._handleIndexChange}
                        initialLayout={initialLayout}
                    />
                </View>
                <View style={styles.saveButtonStyle}>
                    <TouchableHighlight style={styles.orangeButton} onPress={() => {
                        this.save()
                    }}>
                        <Text style={{ color: '#ffffff', fontFamily: 'Montserrat-Bold' }}>Registrami</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    personalDate: {
        backgroundColor: '#25a588',
        alignItems: 'center',
        height: '100%',
    },
    labelStyle: {
        color: '#b8e1d8',
        marginTop: '3%'
    },
    viewTextInputStyle: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#b8e1d8',
        borderBottomWidth: 1,
    },

    textInputStyle: {
        //marginTop: -10,
        marginBottom: '1%',
        width: '100%',
        fontFamily: 'Montserrat-Bold',
        color: '#ffffff',
        textAlign: 'center',

    },

    badgeView: {
        backgroundColor: '#ffffff',
        height: 30,
        marginTop: '1%',
        width: 30,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '3%',
    },

    badgeText: {
        fontSize: 25,
        color: '#3a3a39',
        fontWeight: '400',
    },

    viewSpecialization: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#b8e1d8',
        borderBottomWidth: 1,
        flexDirection: 'row',
    },
    textSpecialization: {
        marginBottom: '1%',
        width: '50%',
        fontFamily: 'Montserrat-Bold',
        color: '#ffffff',
        textAlign: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    uploadAvatar: {
        width: 100,
        height: 100,
        backgroundColor: 'white',
        marginTop: '3%',
        borderRadius: 50
    },
    orangeButton: {
        width: '80%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e94e1b',
        padding: 10,
        marginTop: '3%',
        borderRadius: 25
    },

    saveButtonStyle: {
        height: '10%',
        alignItems: 'center',
        backgroundColor: '#25a588'
    }

});