import React from 'react';
import { View, ScrollView, Modal } from 'react-native';
import Header from '../components/Header';
import ImageBox from '../components/ImageBox';
import Search from '../components/Search';


export default class EventDetail extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            routes: [
                { key: 'first', title: 'STRUMENTI' },
                { key: 'second', title: 'NOTIFICHE' },
            ],
            modalVisible: false,
            data: ''
        };

        this.didPressSearch = this.didPressSearch.bind(this);
        this.didPressClose = this.didPressClose.bind(this)
    }

    didPressSearch() {
   
        this.setState({ modalVisible: true });
    }

    didPressClose() {
   
        this.setState({ modalVisible: false });
    }

    render(){
    let params= this.props.navigation.state.params
    return (
        <View style={{height:'100%',width:'100%'}}>
            <Header navigation={this.props.navigation} searchCallback={this.didPressSearch} />
                    <Modal
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setState({
                                modalVisible: false
                            })
                        }}>
                        <Search navigation={this.props.navigation} closeCallback={this.didPressClose} />
                    </Modal>
            <ImageBox params={params} navigation={this.props.navigation}/>
        </View>
    );
}
};
