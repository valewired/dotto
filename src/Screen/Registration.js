import React from 'react';
import { ScrollView, View } from 'react-native';
import RegistrationForm from '../components/RegistrationForm';

export default class Registration extends React.Component{

    constructor(props){
        super(props)
    }

    render(){
        return (
            <View>
                <RegistrationForm  navigation = {this.props.navigation}/>
            </View>
        )
    }
}