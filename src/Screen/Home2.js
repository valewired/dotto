


import * as React from 'react';
import { View, StyleSheet, Dimensions, Text, Image, Modal, StatusBar } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import { NavigationActions } from 'react-navigation'
import Header from '../components/Header';
import Buttons from '../components/Buttons';
import Box from '../components/Box';
import CentralBox from '../components/CentralBox';
import FooterBox from '../components/FooterBox';
import ButtonsNotifiche from '../components/ButtonsNotifiche';
import CardList from '../components/CardList';
import Search from '../components/Search';
import DefaultPreference from 'react-native-default-preference';

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

class FirstRoute extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    return (<View style={{ height: '100%' }}>
      <Box navigation={this.props.navigation} />
      <CentralBox />
    </View>);
  }
}

class SecondRoute extends React.Component {

  render() {
    return (
      <View style={{ height: '100%' }}>
        <CardList />
      </View >
    )
  }
};


export default class TabViewHome extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      routes: [
        { key: 'first', title: 'STRUMENTI' },
        { key: 'second', title: 'NOTIFICHE' },
      ],
      modalVisible: false,
      modalVisibleProfile: false
    };
    this.didPressSearch = this.didPressSearch.bind(this);
    this.didPressClose = this.didPressClose.bind(this)
    this.didPressProfile = this.didPressProfile.bind(this);
    this.didPressFooterButton = this.didPressFooterButton.bind(this);
  }

  

  renderLabel = ({ route }) => (
    <View>
      <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 11, color: '#3a3a39' }}>{route.title}</Text>
    </View>
  )

  renderBadge = ({ route }) => {
    if (route.key === 'second') {
      return (
        <View style={{ marginTop: '11%', marginRight: '7%' }}>
          <Image style={{ height: 20, width: 40 }} source={require('../../assets/images/icon-359.png')} />
        </View>
      );
    }
    return null;
  };

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = (props) =>
    <TabBar
      {...props}
      tabStyle={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}
      renderLabel={this.renderLabel}
      renderBadge={this.renderBadge}
      style={{ backgroundColor: '#ffffff', height: '8%', justifyContent: 'center' }}
      indicatorStyle={{
        borderBottomWidth: 3,
        borderBottomColor: '#25A588'
      }}

    />;

  _renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  didPressSearch() {

    this.setState({ modalVisible: true });
  }

  didPressClose() {
    this.setState({ modalVisible: false });

  }

  didPressProfile() {
    this.setState({ modalVisibleProfile: true });

  }

  didPressProfileClose() {
    
    this.setState({ modalVisibleProfile: false });


  }

  didPressFooterButton(buttonType) {
    
    this.props.navigation.navigate('FilePage', String(buttonType));
  }

  render() {
    return (
      <View style={{ height: '100%' }}>
        <StatusBar
          backgroundColor="#1A7B65"
          barStyle="light-content"
        />
        <Header navigation={this.props.navigation} searchCallback={this.didPressSearch} profileCallback={this.didPressProfile} />
        <Modal
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setState({ modalVisible: false })
          }}>
          <Search navigation={this.props.navigation} closeCallback={this.didPressClose} />
        </Modal>
        <TabViewAnimated
          style={styles.container}
          navigationState={this.state}
          renderScene={this._renderScene}
          renderHeader={this._renderHeader}
          onIndexChange={this._handleIndexChange}
          initialLayout={initialLayout}
        />
        <FooterBox pressCallback={this.didPressFooterButton} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});